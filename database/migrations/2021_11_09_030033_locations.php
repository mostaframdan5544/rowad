<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Locations extends Migration
{
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');

            $table->nullableMorphs('locationable');
            $table->float('longitude');
            $table->float('latitude');

            $table->text('address');
        });
    }

    public function down()
    {
        Schema::dropIfExists('locations');
    }

}
