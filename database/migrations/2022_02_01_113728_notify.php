<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Notify extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notify', function (Blueprint $table) {
            $table->increments('id');
            $table->nullableMorphs('notifyable');
            $table->integer('notification_id')->unsigned();
            $table->foreign('notification_id')->references('id')->on('notifications')->onDelete('cascade');
            $table->integer('ask_for_driver_response_id')->unsigned()->nullable();
            $table->foreign('ask_for_driver_response_id')->references('id')->on('ask_for_driver_responses')->onDelete('cascade');
            $table->boolean('from_dashboard')->default(1);
            $table->boolean('is_seen')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
