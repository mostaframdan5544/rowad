<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Orders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('driver_id')->unsigned()->nullable();
            $table->foreign('driver_id')->references('id')->on('drivers')->onDelete('cascade');

            $table->integer('provider_id')->unsigned()->nullable();
            $table->foreign('provider_id')->references('id')->on('providers')->onDelete('cascade');

            $table->enum('status', ['waiting','processing','delivering','finished', 'canceled', 'refused', 'timeout'])->default('waiting');

            $table->integer('from_locations_id')->unsigned()->nullable();
            $table->foreign('from_locations_id')->references('id')->on('locations')->onDelete('cascade');

            $table->integer('to_locations_id')->unsigned()->nullable();
            $table->foreign('to_locations_id')->references('id')->on('locations')->onDelete('cascade');
            
            $table->double('time_out')->nullable();
            // $table->integer('voucher_id')->unsigned()->nullable();
            // $table->foreign('voucher_id')->references('id')->on('vouchers')->onDelete('cascade');
            $table->boolean('ask_for_driver')->default(0);
            $table->text('nots')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
