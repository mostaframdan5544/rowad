<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AppSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('action_by')->unsigned();

            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->integer('price_per_km')->default(0);
            $table->double('delivery_range')->default(1);
            $table->double('vat')->default(0);
            $table->text('facebook')->nullable();
            $table->text('twitter')->nullable();
            $table->text('snapchat')->nullable();
            $table->string('logo')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_settings');
    }
}
