<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medias', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('action_by')->unsigned()->nullable();
            $table->foreign('action_by')->references('id')->on('admins')->onDelete('cascade');
            $table->enum('type', ['image','video'])->default('image');
            $table->morphs('mediable');
            $table->enum('display_at', ['profile_image','background'])->nullable();
            $table->string('path');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medias');
    }
}
