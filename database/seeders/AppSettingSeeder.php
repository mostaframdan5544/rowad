<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\AppSetting;
use App\Models\AppSettingTranslation;

class AppSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	 
        $appsets = AppSetting::create([
            'action_by'=> 1,
            'email'     => 'rowad@mgdsoft.com',
            'phone'     => '00000000000',
        ]);

        AppSettingTranslation::create([
            'app_setting_id'=> $appsets->id,
            'locale'     => 'en',
            'name' => 'Rowad',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum exercitationem quis delectus architecto facere eveniet non et possimus, praesentium labore necessitatibus laboriosam enim, quo, sit est? Minima, voluptatibus fugiat.',
            'about_us' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum exercitationem quis delectus architecto facere eveniet non et possimus, praesentium labore necessitatibus laboriosam enim, quo, sit est? Minima, voluptatibus fugiat.',
            'terms' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum exercitationem quis delectus architecto facere eveniet non et possimus, praesentium labore necessitatibus laboriosam enim, quo, sit est? Minima, voluptatibus fugiat.',
        ]);

        AppSettingTranslation::create([
            'app_setting_id'=> $appsets->id,
            'locale'     => 'ar',
            'name' => 'رواد',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum exercitationem quis delectus architecto facere eveniet non et possimus, praesentium labore necessitatibus laboriosam enim, quo, sit est? Minima, voluptatibus fugiat.',
            'about_us' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum exercitationem quis delectus architecto facere eveniet non et possimus, praesentium labore necessitatibus laboriosam enim, quo, sit est? Minima, voluptatibus fugiat.',
            'terms' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum exercitationem quis delectus architecto facere eveniet non et possimus, praesentium labore necessitatibus laboriosam enim, quo, sit est? Minima, voluptatibus fugiat.',
        ]);

    }
}
