<?php

return [
    /**
     * Control if the seeder should create a user per role while seeding the data.
     */
    'create_users' => true,

    /**
     * Control if all the laratrust tables should be truncated before running the seeder.
     */
    'truncate_tables' => true,

    'role_structure' => [
        'super' => [
            'admins'        => 'c,r,u,d',
            'roles'         => 'c,r,u,d',
            'users'         => 'c,r,u,d',
            'drivers'       => 'c,r,u,d',
            'providers'     => 'c,r,u,d',
            'appsettings'   => 'c,r,u,d',
            'orders'        => 'r',
            'rates'         => 'r',
        ],
    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
   ]

];
