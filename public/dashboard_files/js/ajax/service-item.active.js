// active service item toggle
$(".service-item-active-toggle").on('click', function(e) {
    let service_item_id = $(this).attr('data-serviceItemId')
    let active = 0
    if ($(this).is(':checked')) { active = 1 } else { active = 0 }

    let ajaxData = {
        "service_item_id": service_item_id,
        "active": active,
    }
    let url = $(this).attr('data-route');

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    $.ajax({
        type: "POST",
        url: url,
        data: ajaxData,
        dataType: 'json',
        contentType: "application/x-www-form-urlencoded",
        success: function(data) {
            if (data.status == "success") {
                Toast.fire({
                    icon: 'success',
                    title: 'Item Active Swaped.'
                })
            } else if (data.status == "error") {
                Toast.fire({
                    icon: 'error',
                    title: 'There Had Been Problem '
                })
            }

        },
        error: function(data) {
            Toast.fire({
                icon: 'error',
                title: 'There Had Been Problem '
            })
        }
    });

})
