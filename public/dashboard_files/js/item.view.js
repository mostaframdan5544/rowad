// active service item toggle
$(".table-item-active-toggle").on('click', function(e) {
    let service_id = $(this).attr('data-serviceId')
    let item_id = $(this).attr('data-itemId')
    let category_id = $(this).attr('data-categorId')
    let unit_id = $(this).attr('data-unitId')

    let active = 0
    if ($(this).is(':checked')) { active = 1 } else { active = 0 }

    let ajaxData = {
        "service_id": service_id,
        "item_id": item_id,
        "category_id": category_id,
        "unit_id": unit_id,
        "active": active,
    }

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });
    $.ajax({
        type: "POST",
        url: "/service-items/toggle_active",
        data: ajaxData,
        dataType: 'json',
        contentType: "application/x-www-form-urlencoded",
        success: function(data) {
            console.log(data.status)
            console.log(data)
            if (data.status == "success") {
                Toast.fire({
                    icon: 'success',
                    title: 'Item Active Swaped.'
                })
            } else if (data.status == "error") {
                Toast.fire({
                    icon: 'error',
                    title: 'There Had Been Problem '
                })
            }

        },
        error: function(data) {
            Toast.fire({
                icon: 'error',
                title: 'There Had Been Problem '
            })
        }
    });

})