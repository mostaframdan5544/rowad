$(function() {
    $('.select-search').select2()


    // healthy check enable input for add item
    $("#add-item-healthy-checkbox").on('click', function() {
        if ($(this).is(':checked'))
            $('#add-item-healthy-value-input').prop("disabled", false);
        else
            $('#add-item-healthy-value-input').prop("disabled", true);
    })

    // healthy check enable input for add item
    $('#add-item-discount-checkbox').on('click', function() {
        if ($(this).is(':checked')) {
            $('#add-item-service-discount-end-date-container').removeClass("hidden");
            $('#add-item-service-discount-type').prop("disabled", false);
            $('#add-item-service-discount-value').prop("disabled", false);

        } else {
            $('#add-item-service-discount-end-date-container').addClass("hidden");
            $('#add-item-service-discount-type').prop("disabled", true);
            $('#add-item-service-discount-value').prop("disabled", true);
        }
    })
})