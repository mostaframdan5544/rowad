// healthy check enable input for add item
function enableHealthyValue() {
    if ($("#add-item-healthy-checkbox").is(':checked'))
        $('#add-item-healthy-value-input').prop("disabled", false);
    else
        $('#add-item-healthy-value-input').prop("disabled", true);
}

// healthy check enable input for edit item
function enableEditHealthyValue() {
    if ($("#item-edit-healthy").is(':checked'))
        $('#item-edit-healthy-value').prop("disabled", false);
    else
        $('#item-edit-healthy-value').prop("disabled", true);
}

// // active service item toggle
// $(".table-item-active-toggle").on('click', function(e) {
//     let service_id = $(this).attr('data-serviceId')
//     let item_id = $(this).attr('data-itemId')
//     let category_id = $(this).attr('data-categorId')
//     let unit_id = $(this).attr('data-unitId')

//     let active = 0
//     if ($(this).is(':checked')) { active = 1 } else { active = 0 }

//     let ajaxData = {
//         "service_id": service_id,
//         "item_id": item_id,
//         "category_id": category_id,
//         "unit_id": unit_id,
//         "active": active,
//     }

//     const Toast = Swal.mixin({
//         toast: true,
//         position: 'top-end',
//         showConfirmButton: false,
//         timer: 3000
//     });
//     $.ajax({
//         type: "POST",
//         url: "/service-items/toggle_active",
//         data: ajaxData,
//         dataType: 'json',
//         contentType: "application/x-www-form-urlencoded",
//         success: function(data) {
//             console.log(data.status)
//             console.log(data)
//             if (data.status == "success") {
//                 Toast.fire({
//                     icon: 'success',
//                     title: 'Item Active Swaped.'
//                 })
//             } else if (data.status == "error") {
//                 Toast.fire({
//                     icon: 'error',
//                     title: 'There Had Been Problem '
//                 })
//             }

//         },
//         error: function(data) {
//             Toast.fire({
//                 icon: 'error',
//                 title: 'There Had Been Problem '
//             })
//         }
//     });

// })

// fill edit service-item modal when click on edit icon
$('.edit-item-icon').on('click', function() {
    let serviceItem = $(this).attr('data-serviceItem')
    let item_object = JSON.parse(serviceItem)
    $('.item-edit-unit-' + item_object.unit_id).attr("selected", true);
    $('.item-edit-category-' + item_object.category_id).attr("selected", true);
    $('#item-edit-cost').val(item_object.cost)
    $('#item-edit-item-id').val(item_object.id)


    if (item_object.service_item_active) {
        $('#item-edit-active').prop("checked", true);
    } else {
        $('#item-edit-active').prop("checked", false);
    }
    if (item_object.healthy) {
        $('#item-edit-healthy').prop("checked", true);
        $('#item-edit-healthy-value').prop("disabled", false);
        $('#item-edit-healthy-value').val(item_object.healthy_value)
    } else {
        $('#item-edit-healthy').prop("checked", false);
        $('#item-edit-healthy-value').prop("disabled", true);
        $('#item-edit-healthy-value').val("")
    }
    $("#item-edit-modal").modal();
})