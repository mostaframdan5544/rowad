@extends('dashboard.layouts.app')

@section('title', __('dash.'.$module_name_plural))

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <!-- Content Header (Page header) -->
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>@lang('dash.' . $module_name_plural)</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard.home') }}">@lang('dash.home')</a></li>
                            <li class="breadcrumb-item active">@lang('dash.' . $module_name_plural)</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>
        <section class="content-header">
            <div class="container-fluid row d-flex justify-content-center ">
                @if(session('success'))
                    <div class="alert alert-success col-sm-6 text-center" role="alert">
                        {!! session('success') !!}
                    </div>
                @endif

                @if(session('error'))
                    <div class="alert alert-danger col-sm-6 text-center" role="alert">
                        {!! session('error') !!}
                    </div>
                @endif
            </div>
        </section>
        <!-- /.session-messages -->
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <!-- /.card-header -->
                            <div class="card-body">
                                {!! $dataTable->table(['class' => 'table table-bordered']) !!}
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section><!-- /.content -->
    </div>
@endsection

@push('header-after-theme')
    <!-- start datatables style for yajar package -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    <!-- end  datatables style for yajar package -->
@endpush

@push('footer-after-jquery')
    <!-- start datatables script for yajar package -->
    <!-- jQuery -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" defer ></script>

    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js" defer ></script>
    <script src="{{ asset('vendor/datatables/buttons.server-side.js') }}" defer ></script>
    {!! $dataTable->scripts() !!}
@endpush

