@extends('dashboard.layouts.app')

@section('title', __('dash.add') .' '. __('dash.'.$module_name_plural))

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid row d-flex justify-content-center ">
                @if(session('success'))
                <div class="alert alert-success col-sm-6 text-center" role="alert">
                    {!! session('success') !!}
                </div>
                @endif

                @if(session('error'))
                <div class="alert alert-danger col-sm-6 text-center" role="alert">
                    {!! session('error') !!}
                </div>
                @endif

                @if(count($errors->getMessages()) > 0)
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <strong>@lang('dash.validation_errors')</strong>
                    <ul>
                        @foreach($errors->getMessages() as $errorMessages)
                            @foreach($errorMessages as $errorMessage)
                                <li>
                                    {{ $errorMessage }}
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </li>
                            @endforeach
                        @endforeach
                    </ul>
                </div>                           
                @endif

            </div>
        </section><!-- /.session-messages -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- right column -->
                    <div class="col-md-12">
                        <!-- general form elements disabled -->
                        <div class="card card-success">
                            <div class="card-header ">
                                <h3 class="card-title" > @lang('dash.create') @lang('dash.'.$module_name_singular) </h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <form   class="form row" enctype="multipart/form-data" method="POST"
                                        action="{{ route('dashboard.'.$module_name_plural.'.store') }}">
                                    @method('POST')

                                    @include('dashboard.'.$module_name_plural.'.form')

                                    <div class="form-group col-md-6">
                                        <button data-repeater-create="" class="btn btn-primary">
                                            <i class="fa fa-plus"></i> @lang('dash.add')
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col   -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@push('header-after-icons')
    <link rel="stylesheet" href="{{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
@endpush
@push('footer-after-bootstrap')
    <script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
@endpush
