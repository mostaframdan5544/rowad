@extends('dashboard.layouts.app')

@section('title', __('dash.profile'))

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1>@lang('dash.profile')</h1>
            </div>
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.home') }}">@lang('dash.home')</a></li>
                <li class="breadcrumb-item active">@lang('dash.my_profile')</li>
            </ol>
            </div>
        </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content-header">
        <div class="container-fluid row d-flex justify-content-center ">
            @if(session('success'))
            <div class="alert alert-success col-sm-6 text-center" role="alert">
                {!! session('success') !!}
            </div>
            @endif

            @if(session('error'))
            <div class="alert alert-danger col-sm-6 text-center" role="alert">
                {!! session('error') !!}
            </div>
            @endif
        </div>
        @if(count($errors->getMessages()) > 0)
            <div class="alert alert-danger alert-dismissible" role="alert">
                <strong>@lang('dash.validation_errors')</strong>
                <ul>
                    @foreach($errors->getMessages() as $errorMessages)
                        @foreach($errorMessages as $errorMessage)
                            <li>
                                {{ $errorMessage }}
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </li>
                        @endforeach
                    @endforeach
                </ul>
            </div>                           
        @endif
    </section><!-- /.session-messages -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <!-- Profile Image -->
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle"
                                src="{{ asset(auth()->user()->image_path) }}"
                                alt="User profile picture">
                        </div>

                        <h3 class="profile-username text-center">{{ auth()->user()->name }}</h3>

                        <p class="text-muted text-center">
                            <spane class="badge bg-success text-uppercase">@if(is_null($row->adminRole)) @lang('dash.not_set_yet') @else {{$row->adminRole->role->display_name}} @endif</span>
                        </p>

                        <ul class="list-group list-group-unbordered mb-3">
                            {{-- <li class="list-group-item">
                                <b>@lang('dash.drivers')</b> <a class="float-right">{{ \App\Models\Driver::count() }}</a>
                            </li>
                            <li class="list-group-item">
                                <b>@lang('dash.providers')</b> <a class="float-right">{{ \App\Models\Provider::count() }}</a>
                            </li>  #Remove after merge with users branch --}}
                            <li class="list-group-item">
                                <b>@lang('dash.clients')</b> <a class="float-right">{{ \App\Models\User::count() }}</a>
                            </li>
                        </ul>
                        <a  href="#" class="btn btn-primary btn-block"
                            data-toggle="modal" data-target="#changePassword"><b> <i class="fa fa-cog"></i> @lang('dash.change_password')</b></a>
                            @component('dashboard.components.changePassword') @endcomponent
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <!-- About App Box -->
                    @php 
                        $settings = \App\Models\AppSetting::first();
                    @endphp
                    <div class="card card-primary">
                        <div class="card-header">
                        <h3 class="card-title">@lang('dash.about_app')</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                        <strong><i class="fas fa-book mr-1"></i>@lang('dash.description')</strong>

                        <p class="text-muted">
                            @if( is_null($settings) ) @lang('dash.not_set_yet')  @else {!! $settings->description !!} @endif
                        </p>

                        <hr>

                        <strong><i class="fas fa-map-marker-alt mr-1"></i>@lang('dash.location')</strong>

                        <p class="text-muted">@if( is_null($settings) || is_null($settings->location) ) 
                            @lang('dash.not_set_yet')  @else {{ $settings->location->address }} 
                            @endif</p>
                        <hr>

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header p-2">
                            <ul class="nav nav-pills">
                                <li class="nav-item active"><a class="nav-link active" 
                                    href="#edit_profile" data-toggle="tab">@lang('dash.edit_profile')</a></li>
                            </ul>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="edit_profile">
                                    <form   class="form-horizontal" method="POST" enctype="multipart/form-data" 
                                            action="{{ route('dashboard.admins.update', ['admin' => $row->id]) }}"
                                    >
                                        @method('PUT')
                                        {{ csrf_field() }}
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 col-form-label">@lang('dash.name')</label>
                                            <div class="col-sm-10">
                                                <input  type="text" class="form-control" id="inputName" placeholder="@lang('dash.name')"
                                                        value="{{auth()->user()->name}}" name="name">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputEmail" class="col-sm-2 col-form-label">@lang('dash.email')</label>
                                            <div class="col-sm-10">
                                                <input  type="email" class="form-control" id="inputEmail" placeholder="@lang('dash.email')"
                                                        value="{{auth()->user()->email}}" name="email">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputPhone" class="col-sm-2 col-form-label">@lang('dash.phone')</label>
                                            <div class="col-sm-10">
                                                <input  type="text" class="form-control" id="inputPhone" placeholder="@lang('dash.phone')"
                                                        value="{{auth()->user()->phone}}" name="phone">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label" for="inputProfileImage">@lang('dash.profile_image')</label>
                                            <div class="col-sm-10">
                                                <input type="file" name="profile_image" id="inputProfileImage" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="offset-sm-2 col-sm-10">
                                                <button type="submit" class="btn btn-danger">
                                                    <i class="fas fa-share-square"></i> @lang('dash.submit')</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection

@push('footer-after-jquery')
    <!-- AdminLTE App -->
    <script src="{{ asset('dashboard_files/dist/js/adminlte.min.js') }}" defer></script>

    <!-- Session change password error message -->
    @if( Session::has('changePasswordErrorMessage') )
        <script>
            $(document).ready(function(){
                jQuery.noConflict();
                $('#changePassword').modal({show: true});
            });
        </script>
    @endif
@endpush