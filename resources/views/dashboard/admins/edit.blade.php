@extends('dashboard.layouts.app')

@section('title', __('dash.update') .' '. __('dash.'.$module_name_plural))

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid row d-flex justify-content-center ">
                @if(session('message'))
                    <div class="alert col-sm-6 text-center alert-{{session('message_type') == 'success' ?  'success'  :  'warning' }}"
                        role="alert">
                        {{session('message')}}
                    </div>
                @endif
            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!--left column -->
                    <div class="col-md-3">
                        <!-- Profile Image -->
                        <div class="card card-primary card-outline">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                <img class="profile-user-img img-fluid img-circle"
                                    src="{{asset($row->image_path)}}"
                                    alt="User profile picture">
                                </div>

                                <h3 class="profile-username text-center">{{$row->name}}</h3>
                                <p class="text-muted text-center">
                                    <spane class="badge bg-success text-uppercase">@if(is_null($row->adminRole)) @lang('dash.not_set_yet') @else {{$row->adminRole->role->display_name}} @endif</span>
                                </p>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                    <!-- right column -->
                    <div class="col-md-9">
                        <!-- general form elements disabled -->
                        <div class="card card-success">
                            <div class="card-header ">
                                <h3 class="card-title"> @lang('dash.update') @lang('dash.'.$module_name_singular) </h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <form class="form row" method="POST" enctype="multipart/form-data"
                                    action="{{ route('dashboard.'.$module_name_plural.'.update', $row->id) }}">
                                    @method('PUT')
                                    @include('dashboard.'.$module_name_plural.'.form')

                                    <div class="form-group col-md-6">
                                        <button data-repeater-create="" class="btn btn-primary">
                                            <i class="fa fa-cog"></i> @lang('dash.update')
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/.col-->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@push('header-after-icons')
    <link rel="stylesheet" href="{{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
@endpush
@push('footer-after-bootstrap')
    <script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
@endpush