{{ csrf_field() }}

<div class="form-group col-md-6 mb-2">
    <div class="form-group">
        <label class="bmd-label-floating">@lang('dash.name')</label>
        <input name="name" class="form-control @error('name') is-invalid  @enderror" value="{{ isset($row) ? $row->name : old('name') }}">
        @error('name')
        <small class=" text text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </small>
        @enderror
    </div>
</div> {{-- name --}}

<div class="form-group col-md-6 mb-2">
    <div class="form-group">
        <label class="bmd-label-floating">@lang('dash.email')</label>
        <input name="email" class="form-control @error('email') is-invalid  @enderror" value="{{ isset($row) ? $row->email : old('email') }}">
        @error('email')
        <small class=" text text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </small>
        @enderror
    </div>
</div> {{-- email --}}

<div class="form-group col-md-6 mb-2">
    <div class="form-group">
        <label class="bmd-label-floating">@lang('dash.phone')</label>
        <input name="phone" class="form-control @error('phone') is-invalid  @enderror" value="{{ isset($row) ? $row->phone : old('phone') }}">
        @error('phone')
        <small class=" text text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </small>
        @enderror
    </div>
</div> {{-- phone --}}

<div class="form-group col-md-4 mb-2">
    <label class="col-sm-6 col-form-label" for="profile_image">@lang('dash.profile_image')</label>
    <div class="col-sm-10 col-lg-10 col-md-2">
        <input type="file" name="profile_image" id="profile_image" class="form-control @error('profile_image') is-invalid @enderror">
    </div>
    @error('profile_image')
    <small class=" text text-danger" role="alert">
        <strong>{{ $message }}</strong>
    </small>
    @enderror
</div> {{-- profile_image --}}

@if(!isset($row))
    <div class="form-group col-md-6 mb-2">
        <div class="form-group">
            <label class="bmd-label-floating">@lang('dash.password')</label>
            <input name="password" type="password" class="form-control @error('password') is-invalid  @enderror">
            @error('password')
            <small class=" text text-danger" role="alert">
                <strong>{{ $message }}</strong>
            </small>
            @enderror
        </div>
    </div> {{-- password --}}
    <div class="form-group col-md-6 mb-2">
        <div class="form-group">
            <label class="bmd-label-floating">@lang('dash.confirm_password')</label>
            <input id="password-confirm" type="password" class="form-control @error('password_confirmation') is-invalid  @enderror" name="password_confirmation" required autocomplete="new-password">
        </div>
    </div> {{-- confirm_password --}}
@endif

<div class="form-check form-group col-md-4 mb-2">
    <input type="checkbox" class="form-check-input" id="isActiveCheckbox" name="is_active"
           {{ isset($row) && $row->is_active ? 'checked' : '' }} >
    <label class="form-check-label" for="isActiveCheckbox">@lang('dash.is_active')</label>
    @error("is_active")
    <span class="text-danger">{{$message }}</span>
    @enderror
</div>  {{-- is_active --}}