{{ csrf_field() }}

<div class="form-group col-md-6 mb-2">
    <div class="form-group">
        <label class="bmd-label-floating">@lang('dash.name')</label>
        <input name="name" class="form-control @error('name') is-invalid  @enderror" value="{{ isset($row) ? $row->name : old('name') }}">
        @error('name')
        <small class=" text text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </small>
        @enderror
    </div>
</div>

<div class="form-group col-md-6 mb-2">
    <div class="form-group">
        <label class="bmd-label-floating">@lang('dash.description')</label>
        <input name="description" class="form-control @error('description') is-invalid  @enderror" value="{{ isset($row) ? $row->description : old('description') }}">
        @error('description')
        <small class=" text text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </small>
        @enderror
    </div>
</div>

@php
    $models = config('laratrust_seeder.role_structure.super');
    $maps = ['create', 'read', 'update', 'delete'];
@endphp
@foreach ($models as $model => $detail)
    <div class="list-group col-md-3 mb-2">
        <div class="form-group">
            {{-- route('dashboard.' . $model . '.index') --}}
            <a href="#" class="list-group-item active">
                @lang('dash.'.$model)
            </a>
            @foreach ($maps as $map)
                <label>@lang('dash.'.$map)</label>
                <input type="checkbox" style="height: 20px" class="form-control" name="permissions[]"
                       {{ isset($row) && $row->hasPermission($model . '-' . $map) ? 'checked' : '' }}
                       value="{{ $model . '-' . $map }}">
            @endforeach
        </div>
    </div>
@endforeach
