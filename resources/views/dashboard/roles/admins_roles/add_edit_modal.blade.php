<!-- Modal Users Roles -->
<div    class="modal fade" id="{{ isset($row) ? 'edit_admin_role_'.$row->user_id.$row->role_id : 'add_admin_role' }}"
        tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">

        <div class="modal-content">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('dash.'.$module_name_singular)</h4>
                    <a class="close btn btn-sm mb-0" data-dismiss="modal" aria-label="Close">×</a>
                </div>

                <div class="card-content collpase show">
                    <div class="card-body">
                        <div class="card-text">
                            <p class="card-text" style="margin-bottom: 1.25rem !important;">@lang('dash.add_admin_role_hint')</p>
                        </div>

                        <div class="container-fluid row d-flex justify-content-center">
                            @if( Session::has('createAdminRoleError') )
                                <div class="alert col-sm-6 text-center alert-{{session('message_type') == 'success' ?  'success'  :  'warning' }}"
                                     role="alert">
                                    {{session('createAdminRoleError')}}
                                </div>
                            @elseif( isset($row) && Session::has('updateAdminRoleError') && session("id") == $row->user_id.$row->role_id)
                                <div class="alert col-sm-6 text-center alert-{{session('message_type') == 'success' ?  'success'  :  'warning' }}"
                                     role="alert">
                                    {{session('createAdminRoleError')}}
                                </div>
                            @endif
                        </div><!-- end alert -->

                        <form   class="form" method="POST" id="{{ isset($row) ? 'edit_admin_role_form_'.$row->user_id.$row->role_id : 'add_admin_role_form' }}"
                                action="{{ route('dashboard.adminsroles.createUpdate') }}"
                        >
                            @method('POST')
                            {{ csrf_field() }}
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="text-bold-600 font-medium-2">
                                                @lang('dash.admins')
                                            </div>
                                        	@if( isset($row) )
                                                <input type="hidden" name="user_id" value="{{ $row->admin->id }}">
                                        		<span>{{ $row->admin->name }}</span>
                                        	@else
                                            <select class="select2 form-control mb-1" id="user_id" name="user_id">
                                                @foreach($admins as $admin)
                                                <option value="{{$admin->id}}">
                                                    {{$admin->name.' - '.$admin->phone }}
                                                </option>
                                                @endforeach
                                            </select>
                                        	@endif
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="text-bold-600 font-medium-2">
                                                @lang('dash.roles')
                                            </div>
                                            <select class="select2 form-control" id="role_id" name="role_id">
                                                @foreach($roles as $role)
                                                    <option value="{{$role->id}}" {{ isset($row) && $row->role_id == $role->id  ? 'selected' : '' }}>
                                                        {{$role->display_name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal-footer" style="border-top: none;">
                <div class="form-actions">
                    <button type="button" class="btn btn-outline-warning mr-1" data-dismiss="modal">
                        <i class="ft-x"></i> @lang('dash.cancel')
                    </button>
                    <button type="submit" form="{{ isset($row) ? 'edit_admin_role_form_'.$row->user_id.$row->role_id : 'add_admin_role_form' }}"
                            class="btn btn-outline-primary"
                            onclick="return confirm(`@lang('dash.are_you_sure_save')`)"
                            >
                        <i class="ft-check"></i> @lang('dash.save')
                    </button>
                </div>
            </div>

        </div>

    </div>
</div>
<!-- ./End Modal Users Roles -->
