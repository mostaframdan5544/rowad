@extends('dashboard.layouts.app')

@section('title', __('dash.'.$module_name_plural))

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            @php
                $admins = App\Models\Admin::whereDoesntHave('roles')->get();
                $roles = App\Models\Role::all();
            @endphp

            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>@lang('dash.' . $module_name_plural)</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard.home') }}">@lang('dash.home')</a></li>
                            <li class="breadcrumb-item active">@lang('dash.' . $module_name_plural)</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>

        <section class="content-header">
            <div class="container-fluid row d-flex justify-content-center ">
                @if(session('success'))
                    <div class="alert alert-success col-sm-6 text-center" role="alert">
                        {!! session('success') !!}
                    </div>
                @endif

                @if(session('error'))
                    <div class="alert alert-danger col-sm-6 text-center" role="alert">
                        {!! session('error') !!}
                    </div>
                @endif
            </div>
        </section>
        <!-- /.session-messages -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12" style="margin-bottom: 1.25rem !important;">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add_admin_role"><i
                                class="fa fa-plus"></i> @lang('dash.add') @lang('dash.'.$module_name_singular)</button>
                        @include( 'dashboard.roles.admins_roles.add_edit_modal' )
                    </div>

                    <div class="col-12">
                        <div class="card">
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table class="table table-striped table-bordered zero-configuration" id="myAdminsRolesTable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>@lang('dash.name')</th>
                                        <th>@lang('dash.role')</th>
                                        <th>@lang('dash.created_at')</th>
                                        <th>@lang('dash.actions')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($rows as $index=>$row)
                                            <tr>
                                                <td> {{++$index}} </td>
                                                <td><a title="@lang('dash.admin')" href="#">{{$row->admin->name}}</a></td>
                                                <td> 
                                                    {{$row->role->display_name}}
                                                </td>
                                                <td> {{ Carbon\Carbon::parse($row->created_at)->format('d M Y h:m') }} </td>
                                                <td>
                                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#edit_admin_role_{{$row->user_id.$row->role_id}}">
                                                        <i class="fa fa-cog fa-fw"></i> @lang('dash.update')
                                                    </button>
                                                    @include( 'dashboard.roles.admins_roles.add_edit_modal' )
                                                </td>
                                            </tr>
                                            @empty
                                                <tr><td colspan="5" style="text-align: center;"><h4>@lang('dash.no_records')</h4></td></tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section><!-- /.content -->
    </div>
@endsection

@push('header-after-theme')
    {{-- start datatables style for yajar package --}}
    <!-- Bootstrap CSS -->
    <!-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">

    {{-- end  datatables style for yajar package --}}
@endpush

@push('footer-after-jquery')
    {{-- start datatables script for yajar package --}}
    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" defer></script>
    <!-- BS JavaScript -->
    <!-- <script type="text/javascript" src="{{asset('assets\libs\bootstrap\bootstrap.min.js')}}"></script> -->

    <script>
        $(document).ready( function () {
            $('#myAdminsRolesTable').DataTable();
        } );
    </script>

    @if( Session::has('createAdminRoleError') )
        <script>
            $(document).ready(function(){
                $('#add_admin_role').modal({show: true});
            });
        </script>
    @endif

    @if( Session::has('updateAdminRoleError') )
        <script>
            $(document).ready(function(){
                $('#edit_admin_role_{{session("id")}}').modal({show: true});
            });
        </script>
    @endif
@endpush

