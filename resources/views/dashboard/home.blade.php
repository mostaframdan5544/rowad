@extends('dashboard.layouts.app')

@section('title', __('dash.home'))

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid row d-flex justify-content-center ">
                @if(session('message'))
                    <div class="alert col-sm-6 text-center alert-{{session('message_type') == 'success' ?  'success'  :  'warning' }}"
                         role="alert">
                        {{session('message')}}
                    </div>
                @endif
            </div>
        </section>
        <!-- /.message-session -->

        <!-- Header -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">{{ __('dash.dashboard') }}</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('dashboard.home')}}">{{ __('dash.home') }}</a></li>
                            <li class="breadcrumb-item active">{{ __('dash.dashboard') }}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content px-4">
            <!-- Row -->
            <div class="row">
                @if (auth()->user()->isAbleTo('drivers-read'))
                <!-- drivers -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-secondary">
                        <span class="info-box-icon"><i class="fas fa-info-circle"></i></span>
                        <div class="info-box-content">
                            <a href="#" class='text-white'>
                                <span class="info-box-text">@lang('dash.drivers')</span>
                            </a>
                            <span class="info-box-number">#</span>
                            <div class="progress">
                                <div class="progress-bar" style="width: {{$driverCount}}%"></div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.drivers -->
                @endif
                @if (auth()->user()->isAbleTo('providers-read'))
                <!-- providers -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-secondary">
                        <span class="info-box-icon"><i class="fas fa-info-circle"></i></span>
                        <div class="info-box-content">
                            <a href="#" class='text-white'>
                                <span class="info-box-text">@lang('dash.providers')</span>
                            </a>
                            <span class="info-box-number">#</span>
                            <div class="progress">
                                <div class="progress-bar" style="width: {{$providerCount}}%"></div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.providers -->
                @endif
                @if (auth()->user()->isAbleTo('users-read'))
                <!-- new clients -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-secondary">
                        <span class="info-box-icon"><i class="fas fa-info-circle"></i></span>
                        <div class="info-box-content">
                            <a href="#" class='text-white'>
                                <span class="info-box-text">@lang('dash.clients')</span>
                            </a>
                            <span class="info-box-number">#</span>
                            <div class="progress">
                                <div class="progress-bar" style="width: {{$userCount}}%"></div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.new clients -->
                @endif
                @if (auth()->user()->isAbleTo('orders-read'))
                <!-- orders -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-secondary">
                        <span class="info-box-icon"><i class="fas fa-info-circle"></i></span>
                        <div class="info-box-content">
                            <a href="#" class='text-white'>
                                <span class="info-box-text">@lang('dash.orders')</span>
                            </a>
                            <span class="info-box-number">#</span>
                            <div class="progress">
                                <div class="progress-bar" style="width: {{$orderCount}}%"></div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.orders -->
                @endif
                @if (auth()->user()->isAbleTo('products-read'))
                <!-- products -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-secondary">
                        <span class="info-box-icon"><i class="fas fa-info-circle"></i></span>
                        <div class="info-box-content">
                            <a href="#" class='text-white'>
                                <span class="info-box-text">@lang('dash.products')</span>
                            </a>
                            <span class="info-box-number">#</span>
                            <div class="progress">
                                <div class="progress-bar" style="width: {{$productCount}}%"></div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.products -->
                @endif
            </div> <!-- /.Row -->
        </section>

        <!-- /.content -->
        <section class="content px-4">
            <div class="row">
                @if (auth()->user()->isAbleTo('users-read'))
                <!-- Users Chart -->
                <div class="col-md-12">
                    <!-- LINE CHART -->
                    <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">@lang('dash.clients_chart')</h3>

                        <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                        <canvas id="lineChartUsers" style="height: 250px; min-height: 250px; display: block; width: 316px;" width="316" height="250" class="chartjs-render-monitor"></canvas>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- ./Users Chart -->
                @endif
                @if (auth()->user()->isAbleTo('drivers-read'))
                <!-- Drivers Chart -->
                <div class="col-md-12">
                    <!-- LINE CHART -->
                    <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">@lang('dash.drivers_chart')</h3>

                        <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                        <canvas id="lineChartDrivers" style="height: 250px; min-height: 250px; display: block; width: 316px;" width="316" height="250" class="chartjs-render-monitor"></canvas>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- ./Drivers Chart -->
                @endif
                @if (auth()->user()->isAbleTo('providers-read'))
                <!-- Providers Chart -->
                <div class="col-md-12">
                    <!-- LINE CHART -->
                    <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">@lang('dash.providers_chart')</h3>

                        <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                        <canvas id="lineChartProviders" style="height: 250px; min-height: 250px; display: block; width: 316px;" width="316" height="250" class="chartjs-render-monitor"></canvas>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- ./Providers Chart -->
                @endif
                @if (auth()->user()->isAbleTo('orders-read'))
                <!-- Orders Chart -->
                <div class="col-md-12">
                    <!-- LINE CHART -->
                    <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">@lang('dash.orders_chart')</h3>

                        <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                        <canvas id="lineChartOrders" style="height: 250px; min-height: 250px; display: block; width: 316px;" width="316" height="250" class="chartjs-render-monitor"></canvas>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- ./Orders Chart -->
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-bordered" id="data-table">
                                <thead>
                                    <tr>
                                        <th>@lang('dash.order_id')</th>
                                        <th>@lang('dash.client')</th>
                                        <th>@lang('dash.provider')</th>
                                        <th>@lang('dash.driver')</th>
                                        <th>@lang('dash.status')</th>
                                        <th>@lang('dash.created_at')</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                @endif
                @if (auth()->user()->isAbleTo('products-read'))
                <!-- Products Chart -->
                <div class="col-md-12">
                    <!-- LINE CHART -->
                    <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">@lang('dash.products_chart')</h3>

                        <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                        <canvas id="lineChartProducts" style="height: 250px; min-height: 250px; display: block; width: 316px;" width="316" height="250" class="chartjs-render-monitor"></canvas>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                </div>
                <!-- ./Products Chart -->
                @endif
            </div>
        </section>
        <!-- /.Main-content -->
    </div><!-- /.Content-wrapper -->
@endsection

@push('header-after-icons')
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{ asset('dashboard_files/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('dashboard_files/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{ asset('dashboard_files/plugins/jqvmap/jqvmap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dashboard_files/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
@endpush
@push('footer-after-bootstrap')
    <!-- ChartJS -->
    <script src="{{ asset('dashboard_files/plugins/chart.js/Chart.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ asset('dashboard_files/plugins/sparklines/sparkline.js') }}"></script>
    <!-- JQVMap -->
    <script src="{{ asset('dashboard_files/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('dashboard_files/plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{ asset('dashboard_files/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
    <script src="{{ asset('dashboard_files/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('dashboard_files/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{ asset('dashboard_files/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <!-- Summernote -->
    <script src="{{ asset('dashboard_files/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ asset('dashboard_files/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
@endpush
@push('footer-after-theme')
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{ asset('dashboard_files/dist/js/pages/dashboard.js') }}"></script>
    <script>
        // Users
        var ctx = document.getElementById('lineChartUsers').getContext('2d');
        var lineChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ['Jan.', 'Feb.', 'Mar.', 'Apr.', 'May', 'Jun.',
                         'Jul.', 'Aug.', 'Sep.', 'Oct.', 'Nov.', 'Dec.'
                        ],
                datasets: [{
                    label: '# of Clients',
                    data: @php echo '['.implode(",",$u_arr).']'; @endphp ,
                    backgroundColor: 'rgba(0, 0, 0, 0.1)',
                    borderColor: 'rgba(0, 0, 0, 0.1)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        // Drivers
        var ctx = document.getElementById('lineChartDrivers').getContext('2d');
        var lineChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ['Jan.', 'Feb.', 'Mar.', 'Apr.', 'May', 'Jun.',
                         'Jul.', 'Aug.', 'Sep.', 'Oct.', 'Nov.', 'Dec.'
                        ],
                datasets: [{
                    label: '# of Drivers',
                    data: @php echo '['.implode(",",$d_arr).']'; @endphp ,
                    backgroundColor: 'rgba(0, 0, 0, 0.1)',
                    borderColor: 'rgba(0, 0, 0, 0.1)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        // Providers
        var ctx = document.getElementById('lineChartProviders').getContext('2d');
        var lineChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ['Jan.', 'Feb.', 'Mar.', 'Apr.', 'May', 'Jun.',
                         'Jul.', 'Aug.', 'Sep.', 'Oct.', 'Nov.', 'Dec.'
                        ],
                datasets: [{
                    label: '# of Providers',
                    data: @php echo '['.implode(",",$prov_arr).']'; @endphp ,
                    backgroundColor: 'rgba(0, 0, 0, 0.1)',
                    borderColor: 'rgba(0, 0, 0, 0.1)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        // Orders
        var ctx = document.getElementById('lineChartOrders').getContext('2d');
        var lineChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ['Jan.', 'Feb.', 'Mar.', 'Apr.', 'May', 'Jun.',
                         'Jul.', 'Aug.', 'Sep.', 'Oct.', 'Nov.', 'Dec.'
                        ],
                datasets: [{
                    label: '# of Orders',
                    data: @php echo '['.implode(",",$o_arr).']'; @endphp ,
                    backgroundColor: [
                        'rgba(0, 0, 0, 0.1)'
                    ],
                    borderColor: [
                        'rgba(0, 0, 0, 0.1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        // Products
        var ctx = document.getElementById('lineChartProducts').getContext('2d');
        var lineChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ['Jan.', 'Feb.', 'Mar.', 'Apr.', 'May', 'Jun.',
                         'Jul.', 'Aug.', 'Sep.', 'Oct.', 'Nov.', 'Dec.'
                        ],
                datasets: [{
                    label: '# of Products',
                    data: @php echo '['.implode(",",$prod_arr).']'; @endphp ,
                    backgroundColor: [
                        'rgba(0, 0, 0, 0.1)'
                    ],
                    borderColor: [
                        'rgba(0, 0, 0, 0.1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
@endpush
@push('footer-after-jquery')
    <!-- start datatables script for yajar package -->
    <!-- jQuery -->
    <script src="//code.jquery.com/jquery.js" defer></script>
    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" defer></script>
    <!-- display images -->
    <script>
        $(function() {
            $('#data-table').DataTable({       
                dom: "Blfrtip",
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{!! route('dashboard.order.statistics') !!}',
                },

                type : 'POST',
                columns: [
                    { data: 'order_id', name: 'order_id' },
                    { data: 'client', name: 'client'},
                    { data: 'provider', name: 'provider' },
                    { data: 'driver', name: 'driver'},
                    { data: 'status', name: 'status'},
                    { data: 'created_at', name: 'created_at' },
                ], 
            });
        });
    </script>
@endpush
