{{ csrf_field() }}

<div class="form-group col-md-6 mb-2">
    <div class="form-group">
        <label class="bmd-label-floating">@lang('dash.name')</label>
        <input class="form-control is-invalid  " id="inputTitle"
               placeholder="@lang('dash.name')"
               value="{{ isset($users) ? $users->name : old('name') }}"
               name="name"
        >
        @error('name')
        <span class="text-danger">{{$message}}</span>
        @enderror
    </div>
</div>

<div class="form-group col-md-6 mb-2">
    <label for="">{{__('dash.email')}}</label>
    <input name="email" type="email" id="email" class="form-control @error('email') is-invalid  @enderror"
           @if( !isset($users) ) @endif
           value="{{ isset($users) ? $users->email : old('email') }}"
    >
    @error("email")
    <span class="text-danger">{{$message}}</span>
    @enderror
</div>

<div class="form-group col-md-6 mb-2">
    <label for="">@lang('web.password')</label>
    <input name="password" type="password" id="password" class="form-control @error('password') is-invalid  @enderror"
    >
</div>

<div class="form-group col-md-6 mb-2">
    <label for="">@lang('web.password_confirmation')</label>
    <input name="password_confirmation" type="password"
           class="form-control" >
</div>

<div class="form-group col-md-6 mb-2">
    <div class="form-group">
        <label for="">{{__('dash.phone')}}</label>
        <input name="phone" class="form-control @error('phone') is-invalid  @enderror"
               value="{{ isset($users) ? $users->phone : old('phone') }}">
    </div>
</div>

<div class="form-group col-md-6 mb-2">
    <label for="inputAge">@lang('dash.age')</label>
    <input type="number" id="inputAge" class="form-control" name="age"
           value="{{ isset($users) ? $users->age : old('age') }}">
    @error('age')
    <small class=" text text-danger" role="alert">
        <strong>{{ $message }}</strong>
    </small>
    @enderror
</div>

<div class="form-group col-md-6 mb-2">
    <div class="form-group">
        <label for="">Role</label>
        <select name="account" class="form-control @error('account') is-invalid  @enderror"
                value="{{ isset($users) ? $users->account : old('account') }}">
            <option value="administrator" @if(isset($users) && $users->account == 'administrator') selected  @endif>{{__('dash.administrator')}}</option>
            <option value="sponsor" @if(isset($users) && $users->account == 'sponsor') selected  @endif>{{__('dash.sponsor')}}</option>
            <option value="provider" @if(isset($users) && $users->account == 'provider') selected  @endif>{{__('dash.provider')}}</option>
            <option value="client" @if(isset($users) && $users->account == 'client') selected  @endif>{{__('dash.client')}}</option>
        </select>
    </div>
</div>

<div class="form-group col-md-6 mb-2">
    <label for="inputAge">@lang('dash.profile_image')</label>
    <input type="file" name="profile_image" id="inputProfileImage" class="form-control">
    @error('profile_image')
    <small class=" text text-danger" role="alert">
        <strong>{{ $message }}</strong>
    </small>
    @enderror
</div>

<div class="form-group col-md-4 mb-2">
    <div class="offsetfemale-sm-2 col-sm-10">
        <div class="checkbox">
            <label>
                <input type="radio" name="gender" value="male"
                      @if(isset($users) && $users->gender == 'male') checked @endif > @lang('dash.male')
                <input type="radio" name="gender" value="female"
                       @if(isset($users) && $users->gender == 'female') checked @endif > @lang('dash.female')
            </label>
        </div>
    </div>
</div>

<br>
@push('footer-after-bootstrap')
    <!-- autocomplete google map -->
    <script type="text/javascript"
            src="https://maps.google.com/maps/api/js?key=*****&sensor=false&libraries=places&language=en-AU">
    </script>

@endpush
