@extends('dashboard.layouts.app')

@section('title', __('dash.driver'))

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{__('dash.driver')}}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard.home') }}">@lang('dash.home')</a>
                            </li>
                            <li class="breadcrumb-item active">{{__('dash.driver')}}</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <div class="card-body">
            <table class="table table-striped gy-7 gs-7">
                <thead>
                <tr class="fw-bold fs-6 text-gray-800 border-bottom border-gray-200">
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>name</td>
                    <td>{{$row->name}}</td>
                </tr>

                <tr>
                    <td>email</td>
                    <td>{{$row->email}}</td>
                </tr>

                <tr>
                    <td>phone</td>
                    <td>{{$row->phone}}</td>
                </tr>
                <tr>
                    <td>{{__('trainers::cruds.trainers.logo')}}</td>
                    <td>
                        <img src="" alt="logo" width="100" height="100" class="image-input-circle">
                    </td>
                </tr>
                <tr>
                    <td>Is Active</td>
                    <td>
                        @if($row->is_active)
                            <span class="badge badge-circle badge-success">Yes</span>
                        @else
                            <span class="badge badge-circle badge-danger">No</span>
                        @endif
                    </td>
                </tr>

                <tr>
                    <td>Rate</td>
                    <td>
                        <span>5</span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('footer-after-jquery')
    <!-- AdminLTE App -->
    <script src="{{ asset('dashboard_files/dist/js/adminlte.min.js') }}"></script>

    <!-- Session change password error message -->
    @if( Session::has('changePasswordErrorMessage') )
        <script>
            $(document).ready(function () {
                jQuery.noConflict();
                $('#changePassword').modal({show: true});
            });
        </script>
    @endif
@endpush
