<!-- Start / footer -->
<footer class="main-footer" dir='auto'>
    <strong>Copyright &copy; {{ date('Y') }} <a target='_blank' href="#">MagdSoft</a>.</strong>
    {{ __('home.rights') }}
    <div class="float-right d-none d-sm-inline-block">
        <b>{{ __('home.version') }}</b> 1.0.1
    </div>
</footer>
<!-- End / footer -->
