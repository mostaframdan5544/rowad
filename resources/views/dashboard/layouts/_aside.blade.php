<!-- Start / Sidebar-left -->
{{-- @php $settings = \App\Models\AppSetting::first(); @endphp  #Will be use latter --}}
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('dashboard.home')}}" class="brand-link">
        <img alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"
             {{-- src="{{ is_null($settings)? asset('dashboard_files/dist/img/AdminLTELogo.png') : $settings->logo_path }}">   #Will be use latter --}}
             src="{{ asset('dashboard_files/dist/img/AdminLTELogo.png')}}">
        <span class="brand-text font-weight-light">
            @lang('dash.ROWAD')
        </span>
    </a>

    <!-- Sidebar style="font-size: small;"-->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset(auth()->user()->image_path) }}"
                     class="img-circle elevation-2" alt="@lang('dash.profile_image')">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{auth()->user()->name}}</a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                {{--App Settings - tab  --}}
                @if(auth()->user()->isAbleTo('appsettings-read'))
                    <li class="nav-item">
                        <a href="{{route('dashboard.appsettings.index')}}" class="nav-link">
                            <i class="fas fa-info-circle"></i>
                            <p>@lang('dash.appsettings')</p>
                        </a>
                    </li>
                @endif
                {{-- ./App Settings - tab  --}}

                {{--roles - tab  --}}
                @if (auth()->user()->isAbleTo('roles-read'))
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-info-circle"></i>
                            <p> @lang('dash.roles') <i class="right fas fa-angle-left"></i></p>
                        </a>
                        <ul class="nav nav-treeview">
                            @if (auth()->user()->isAbleTo('roles-create'))
                                <li class="nav-item">
                                    <a href="{{route('dashboard.roles.create')}}" class="nav-link">
                                        <i class="nav-icon fas fa-info-circle"></i>
                                        <p>@lang('dash.add_role')</p>
                                    </a>
                                </li>
                            @endif

                            @if (auth()->user()->isAbleTo('roles-read'))
                            <li class="nav-item">
                                <a href="{{route('dashboard.roles.index')}}" class="nav-link">
                                    <i class="fas fa-info-circle nav-icon"></i>
                                    <p>@lang('dash.all')</p>
                                </a>
                            </li>
                            @endif

                            @if(auth()->user()->isAbleTo('roles-update'))
                                <li class="nav-item">
                                    <a href="{{ route('dashboard.adminsroles.index') }}" class="nav-link">
                                        <i class="nav-icon fas fa-info-circle"></i>
                                        <p>@lang('dash.admins_roles')</p>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif
                {{-- ./roles - tab  --}}

                {{--admins - tab  --}}
                @if (auth()->user()->isAbleTo('admins-read'))
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-info-circle"></i>
                            <p> @lang('dash.admins') <i class="right fas fa-angle-left"></i></p>
                        </a>
                        <ul class="nav nav-treeview">
                            @if (auth()->user()->isAbleTo('admins-create'))
                                <li class="nav-item">
                                    <a href="{{route('dashboard.admins.create')}}" class="nav-link">
                                        <i class="nav-icon fas fa-info-circle"></i>
                                        <p>@lang('dash.add_admin')</p>
                                    </a>
                                </li>
                            @endif
                            @if (auth()->user()->isAbleTo('admins-read'))
                            <li class="nav-item">
                                <a href="{{route('dashboard.admins.index')}}" class="nav-link">
                                    <i class="fas fa-info-circle nav-icon"></i>
                                    <p>@lang('dash.all')</p>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>
                @endif
                {{-- ./admins - tab  --}}
                
                {{--users - tab  --}}
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-info-circle"></i>
                        <p> {{__('dash.users')}} <i class="right fas fa-angle-left"></i></p>
                    </a>

                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('dashboard.users.index')}}" class="nav-link">
                                <i class="fas fa-info-circle nav-icon"></i>
                                <p>{{__('dash.all-users')}}</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{route('dashboard.drivers.index')}}" class="nav-link">
                                <i class="nav-icon fas fa-info-circle"></i>
                                <p>{{__('dash.drivers')}}</p>
                            </a>
                        </li>{{-- ./Sponsors - tab  --}}

                        <li class="nav-item">
                            <a href="{{route('dashboard.providers.index')}}" class="nav-link">
                                <i class="fas fa-info-circle nav-icon"></i>
                                <p>{{__('dash.providers')}}</p>
                            </a>
                        </li>{{-- ./Administrators - tab  --}}

                    </ul>
                </li>
                {{-- ./users - tab  --}}

                {{--orders - tab  --}}
                @if (auth()->user()->isAbleTo('orders-read'))
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-info-circle"></i>
                            <p> @lang('dash.orders') <i class="right fas fa-angle-left"></i></p>
                        </a>
                        <ul class="nav nav-treeview">
                            @if (auth()->user()->isAbleTo('orders-read'))
                                <li class="nav-item">
                                    <a href="{{route('dashboard.orders.index', ['index'=>'waiting']) }}" class="nav-link">
                                        <i class="fas fa-info-circle nav-icon"></i>
                                        <p>@lang('dash.waiting')</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{ route('dashboard.orders.index', ['index'=>'processing']) }}" class="nav-link">
                                        <i class="nav-icon fas fa-info-circle"></i>
                                        <p>@lang('dash.processing')</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{ route('dashboard.orders.index', ['index'=>'delivering']) }}" class="nav-link">
                                        <i class="nav-icon fas fa-info-circle"></i>
                                        <p>@lang('dash.delivering')</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{route('dashboard.orders.index', ['index'=>'finished']) }}" class="nav-link">
                                        <i class="fas fa-info-circle nav-icon"></i>
                                        <p>@lang('dash.finished')</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{ route('dashboard.orders.index', ['index'=>'canceled']) }}" class="nav-link">
                                        <i class="nav-icon fas fa-info-circle"></i>
                                        <p>@lang('dash.canceled')</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{route('dashboard.orders.index', ['index'=>'refused']) }}" class="nav-link">
                                        <i class="fas fa-info-circle nav-icon"></i>
                                        <p>@lang('dash.refused')</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{ route('dashboard.orders.index', ['index'=>'timeout']) }}" class="nav-link">
                                        <i class="nav-icon fas fa-info-circle"></i>
                                        <p>@lang('dash.timeout')</p>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif
                {{-- ./orders - tab  --}}

                {{--rate - tab  --}}
                @if(auth()->user()->isAbleTo('rates-read'))
                    <li class="nav-item">
                        <a href="{{route('dashboard.rates.index')}}" class="nav-link">
                            <i class="fas fa-info-circle"></i>
                            <p>@lang('dash.rates')</p>
                        </a>
                    </li>
                @endif
                {{-- ./rate - tab  --}}
            </ul>
        </nav>
        <!-- /.sidebar-menu -->

    </div>
    <!-- /.sidebar -->
</aside>
<!-- End / Sidebar-left-->
