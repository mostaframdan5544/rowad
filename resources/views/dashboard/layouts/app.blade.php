<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <title>@yield('title')</title>
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('dashboard_files/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- header-after-icons -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

@stack('header-after-icons')

<!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dashboard_files/dist/css/adminlte.min.css') }}">

{{-- ===== include arabic styles ===== --}}
{{-- @if (app()->getLocale() == 'ar')
  <!-- Bootstrap 4 RTL -->
  <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css">
  <!-- Custom style for RTL -->
  <link rel="stylesheet" href="{{ asset('dashboard_files/dist/css/custom.css') }}">
@endif --}}

<!-- header-after-theme -->
@stack('header-after-theme')

<!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <style>
        [class*=sidebar-dark-] .nav-treeview > .nav-item > .nav-link {
            margin-left: 30px;
        }

        [class*=sidebar-dark-] .nav-treeview > .nav-item > .nav-link > i {
            color: #2980b9
        }
    </style>

    <!-- extra_end_style -->
    @stack('extra_end_style')

</head>

<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
@include('dashboard.layouts._navbar')

@include('dashboard.layouts._aside')
<!-- Start Content / App Content -->
@yield('content')
<!-- End Content / App Content -->
    @include('dashboard.partials._session')

    @include('dashboard.layouts._footer')
</div><!-- end of wrapper -->

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous">
</script>

<!-- footer-after-jquery -->
@stack('footer-after-jquery')

<script>
    // set csrf-token
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('dashboard_files/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script> $.widget.bridge('uibutton', $.ui.button) </script>

<!-- Bootstrap 4 -->
<script src="{{ asset('dashboard_files/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>


<!-- footer-after-bootstrap -->
@stack('footer-after-bootstrap')

<!-- AdminLTE App -->
<script src="{{ asset('dashboard_files/dist/js/adminlte.js') }}"></script>

<!-- footer-after-theme -->
@stack('footer-after-theme')

<!-- AdminLTE for demo purposes -->
<script src="{{ asset('dashboard_files/dist/js/demo.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
<script src="https://cdn.tiny.cloud/1/7wkwi7i7280niks2z6rk4z05v4v3yh86unpaosos23tznh9a/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>

<script src="https://cdn.ckeditor.com/4.17.1/standard/ckeditor.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $("body").on("click",'.slider-check-box input',function(e){
        let type= $(this).data('type');
        $.ajax({
        url: "/dashboard/{{Request::segment(2)}}/check/"+type+"/"+$(this).data("id"),
        type: 'GET',
        cache: false,
        contentType: false,
        processData: false,
        error: function(response) {
            alert('error');
            }
        });
    });
</script>
@yield('script')


</body>

</html>
