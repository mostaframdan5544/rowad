<label class="slider-check-box">
    <input type="checkbox" name="checkbox" @if($is_active) checked @endif data-type="is_active" data-id="{{$id}}">
    <span class="check-box-container d-inline-block">
       <span class="circle"></span>
    </span>
</label>

<style>
    .slider-check-box {
        cursor: pointer;
    }

    .slider-check-box .check-box-container {
        background: #fff;
        width: 45px;
        height: 22px;
        border-radius: 20px;
        -webkit-box-shadow: 0 0 5px #f2f2f2;
        box-shadow: 0 0 5px #f2f2f2;
        border: 1px solid #f2f2f2;
        position: relative;
        margin-right: 10px;
        -webkit-transition: all .5s;
        transition: all .5s;
        border: 1px solid lightgray;
    }

    .slider-check-box .check-box-container .circle {
        position: absolute;
        width: 20px;
        height: 20px;
        border-radius: 50%;
        background: #343a40;
        top: 0;
        left: 2px;
        -webkit-transition: all .5s;
        transition: all .5s;
    }

    .slider-check-box input[type=checkbox] {
        display: none;
    }

    .slider-check-box input[type=checkbox]:checked + .check-box-container {
        background: #20c997;
    }

    .slider-check-box input[type=checkbox]:checked + .check-box-container .circle {
        left: 22px;
        background: #fff;
    }
</style>

