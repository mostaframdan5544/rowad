@extends('dashboard.layouts.app')

@section('title', __('dash.'.$module_name_plural))

@section('content')
   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <!-- Content Header (Page header) -->
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>@lang('dash.' . $module_name_plural)</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard.home') }}">@lang('dash.home')</a></li>
                            <li class="breadcrumb-item active">@lang('dash.' . $module_name_plural)</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>
        <section class="content-header">
            <div class="container-fluid row d-flex justify-content-center ">
                @if(session('success'))
                <div class="alert alert-success col-sm-6 text-center" role="alert">
                    {!! session('success') !!}
                </div>
                @endif

                @if(session('error'))
                <div class="alert alert-danger col-sm-6 text-center" role="alert">
                    {!! session('error') !!}
                </div>
                @endif
            </div>
        </section><!-- /.session-messages -->
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    @if (auth()->user()->isAbleTo($module_name_plural.'-update'))
                        <div class="col-md-12" style="margin-bottom: 1.25rem !important;">
                            <a href="{{route('dashboard.'.$module_name_plural.'.edit', ['appsetting'=>$row->id])}}" class="btn btn-info">
                                <i class="fa fa-edit"></i> @lang('dash.update') @lang('dash.'.$module_name_plural) </a>
                        </div>
                    @endif

                    <div class="col-12">
                        {{-- app settings information  --}}
                        <div class="card">
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <!-- Custom Tabs -->
                                        <div class="card">
                                            <div class="card-header d-flex p-0">
                                                <h3 class="card-title p-3">@lang('dash.about_app')</h3>
                                                <ul class="nav nav-pills ml-auto p-2">
                                                    <li class="nav-item"><a class="nav-link active" href="#information" data-toggle="tab">@lang('dash.information')</a></li>
                                                    <li class="nav-item"><a class="nav-link" href="#about" data-toggle="tab">@lang('dash.about')</a></li>
                                                    <li class="nav-item"><a class="nav-link" href="#terms" data-toggle="tab">@lang('dash.terms')</a></li>
                                                    <li class="nav-item dropdown">
                                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
                                                            @lang('dash.more') <span class="caret"></span>
                                                        </a>
                                                        <div class="dropdown-menu">
                                                            <a class="dropdown-item" href="#description" ata-toggle="tab">@lang('dash.description')</a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div><!-- /.card-header -->
                                            <div class="card-body">
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="information">
                                                        @lang('dash.name') :  @if(is_null($row) || is_null($row->name)) @lang('dash.not_set_yet') @else {{$row->name}} @endif <br>
                                                        @lang('dash.email') :  @if(is_null($row) || is_null($row->email)) @lang('dash.not_set_yet') @else {{$row->email}} @endif <br>
                                                        @lang('dash.phone') :  @if(is_null($row) || is_null($row->phone)) @lang('dash.not_set_yet') @else {{$row->phone}} @endif <br>
                                                        @lang('dash.location') : @if(is_null($row) || is_null($row->location)) @lang('dash.not_set_yet')  @else {{ $row->location->address }} @endif <br>
                                                        @lang('dash.facebook') :  @if(is_null($row) || is_null($row->facebook)) @lang('dash.not_set_yet') @else {{$row->facebook}} @endif <br>
                                                        @lang('dash.twitter') :  @if(is_null($row) || is_null($row->twitter)) @lang('dash.not_set_yet') @else {{$row->twitter}} @endif <br>
                                                        @lang('dash.snapchat') :  @if(is_null($row) || is_null($row->snapchat)) @lang('dash.not_set_yet') @else {{$row->snapchat}} @endif <br>
                                                        @lang('dash.price_per_km') :  @if(is_null($row) || is_null($row->price_per_km)) @lang('dash.not_set_yet') @else {{$row->price_per_km.__('dash.km') }} @endif <br>
                                                        @lang('dash.delivery_range') :  @if(is_null($row) || is_null($row->delivery_range)) @lang('dash.not_set_yet') @else {{$row->delivery_range.__('dash.km') }} @endif <br>
                                                    </div>
                                                    <!-- /.tab-pane -->
                                                    <div class="tab-pane" id="about">
                                                        @if(is_null($row) || is_null($row->about_us)) @lang('dash.not_set_yet') @else {!! $row->about_us !!} @endif
                                                    </div>
                                                    <!-- /.tab-pane -->
                                                    <div class="tab-pane" id="terms">
                                                        @if(is_null($row) || is_null($row->terms)) @lang('dash.not_set_yet') @else {!! $row->terms !!} @endif
                                                    </div>
                                                    <!-- /.tab-pane -->
                                                    <div class="tab-pane" id="description">
                                                        @if(is_null($row) || is_null($row->description)) @lang('dash.not_set_yet') @else {!! $row->description !!} @endif
                                                    </div>
                                                    <!-- /.tab-pane -->
                                                </div>
                                                <!-- /.tab-content -->
                                            </div><!-- /.card-body -->
                                        </div>
                                        <!-- ./card -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                        {{-- app settings media  --}}
                        <div class="card">
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <!-- Custom Tabs -->
                                        <div class="card">
                                            <div class="card-header d-flex p-0">
                                                <h3 class="card-title p-3">@lang('dash.app_media')</h3>
                                                <ul class="nav nav-pills ml-auto p-2">
                                                    <li class="nav-item"><a class="nav-link active" href="#images" data-toggle="tab">@lang('dash.images')</a></li>
                                                </ul>
                                            </div><!-- /.card-header -->
                                            <div class="card-body">
                                                <div class="tab-content">
                                                    {{-- images --}}
                                                    <div class="tab-pane active" id="images">
                                                        @if( count($images) )
                                                            <!-- Post -->
                                                            <div class="post">
                                                                <div class="row mb-3">
                                                                    <div class="col-sm-6">
                                                                        <img    class="img-fluid" alt="@lang('dash.photo')"
                                                                                @php
                                                                                    $image = App\Models\Media::where('id',$images[array_rand($images)])->first(); echo 'src="'. asset($image->file_path) .'"';
                                                                                @endphp 
                                                                        >
                                                                    </div>
                                                                    <!-- /.col -->
                                                                    <div class="col-sm-6">
                                                                        <div class="row">
                                                                            <div class="col-sm-6">
                                                                                <img    class="img-fluid mb-3"  alt="@lang('dash.photo')" 
                                                                                        @php
                                                                                            $image = App\Models\Media::where('id',$images[array_rand($images)])->first(); echo 'src="'. asset($image->file_path) .'"'; 
                                                                                        @endphp
                                                                                >
                                                                                <img    class="img-fluid" alt="@lang('dash.photo')"
                                                                                        @php
                                                                                            $image = App\Models\Media::where('id',$images[array_rand($images)])->first(); echo 'src="'. asset($image->file_path) .'"'; 
                                                                                        @endphp
                                                                                >
                                                                            </div>
                                                                            <!-- /.col -->
                                                                            <div class="col-sm-6">
                                                                                <img    class="img-fluid mb-3"  alt="@lang('dash.photo')"
                                                                                        @php
                                                                                            $image = App\Models\Media::where('id',$images[array_rand($images)])->first(); echo 'src="'. asset($image->file_path) .'"'; 
                                                                                        @endphp
                                                                                >
                                                                                <img    class="img-fluid" alt="@lang('dash.photo')"
                                                                                        @php
                                                                                            $image = App\Models\Media::where('id',$images[array_rand($images)])->first(); echo 'src="'. asset($image->file_path) .'"'; 
                                                                                        @endphp 
                                                                                >
                                                                            </div>
                                                                            <!-- /.col -->
                                                                        </div>
                                                                        <!-- /.row -->
                                                                    </div>
                                                                    <!-- /.col -->
                                                                </div>
                                                                <!-- /.row -->
                                                            </div>
                                                            <!-- /.post -->
                                                        @else
                                                            @lang('dash.not_set_yet')
                                                        @endif
                                                    </div>
                                                    <!-- /.tab-pane -->
                                                </div>
                                                <!-- /.tab-content -->
                                            </div><!-- /.card-body -->
                                        </div>
                                        <!-- ./card -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div> <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section><!-- /.content -->
    </div>
@endsection

@push('footer-after-jquery')
    <!-- Bootstrap 4 -->
    <script src="{{ asset('dashboard_files/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dashboard_files/dist/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dashboard_files/dist/js/demo.js') }}"></script>
@endpush
