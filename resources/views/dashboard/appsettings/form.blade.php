{{ csrf_field() }}
<div class="row">
    <div style="display: inline-grid; margin-bottom: 1.25rem !important;" class="col-md-12">
        <a href="{{ route('dashboard.appsettings.show', ['appsetting' => $row->id]) }}" class="btn btn-info">
        <i class="far fa-images"></i> @lang('dash.show_images')</a>
    </div>
    {{-- show all images --}}
    <div class="form-group">
        <label for="inputEmail" class="col-sm-6 col-form-label">@lang('dash.email')</label>
        <div class="col-sm-10 col-lg-10 col-md-2">
            <input  type="email" class="form-control @error('email') is-invalid @enderror" 
                    id="inputEmail" placeholder="@lang('dash.email')"
                    value="{{ isset($row) ? $row->email : old('email') }}" 
                    name="email"
            >
            @error('phone')
                <small class=" text text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </small>
            @enderror
        </div>
    </div> 
    {{-- email --}}
    <div class="form-group">
        <label for="inputPhone" class="col-sm-6 col-form-label">@lang('dash.phone')</label>
        <div class="col-sm-10 col-lg-10 col-md-2">
            <input  type="text" class="form-control @error('email') is-invalid @enderror" 
                    id="inputPhone" placeholder="@lang('dash.phone')"
                    value="{{ isset($row) ? $row->phone : old('phone') }}" 
                    name="phone"
            >
            @error('phone')
                <small class=" text text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </small>
            @enderror
        </div>
    </div>
    {{-- phone --}}
    <div class="form-group">
        <label for="inputAddress">@lang('dash.address')</label>
        <input  type="text" id="inputAddress" class="form-control" name="address"
                value="{{ isset($row) && !is_null($row->location) ? $row->location->address : old('address') }}"
        ></input>
        {{-- <span class="hide map-done" style="color:green;">@lang('dash.data_load_success')</span> --}}
        @error('address')
            <small class=" text text-danger" role="alert">
                <strong>{{ $message }}</strong>
            </small>
        @enderror
        <input type="hidden" name="latitude" id="latitude" value="0" />
        <input type="hidden" name="longitude" id="longitude" value="0" />
    </div>
    {{-- location --}}
    <div class="form-group">
        <label for="inputPricePerKM" class="col-sm-6 col-form-label">@lang('dash.price_per_km')</label>
        <div class="col-sm-10 col-lg-10 col-md-2">
            <input  type="number" class="form-control" id="inputPricePerKM"
                    value="{{$row->price_per_km}}" name="price_per_km">
        </div>
    </div>
    {{-- price_per_km --}}
    <div class="form-group">
        <label for="inputDeliveryRange" class="col-sm-6 col-form-label">@lang('dash.delivery_range')</label>
        <div class="col-sm-10 col-lg-10 col-md-2">
            <input  type="number" class="form-control" id="inputDeliveryRange"
                    value="{{$row->delivery_range}}" name="delivery_range">
        </div>
    </div>
    {{-- delivery_range --}}

    <div class="form-group">
        <label for="inputFacebook" class="col-sm-6 col-form-label">@lang('dash.facebook')</label>
        <div class="col-sm-10 col-lg-10 col-md-2">
            <input  type="text" class="form-control @error('facebook') is-invalid @enderror" 
                    id="inputFacebook" placeholder="@lang('dash.facebook')"
                    value="{{ isset($row) ? $row->facebook : old('facebook') }}" 
                    name="facebook"
            >
            @error('facebook')
                <small class=" text text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </small>
            @enderror
        </div>
    </div>{{-- facebook --}}
    <div class="form-group">
        <label for="inputTwitter" class="col-sm-6 col-form-label">@lang('dash.twitter')</label>
        <div class="col-sm-10 col-lg-10 col-md-2">
            <input  type="text" class="form-control @error('twitter') is-invalid @enderror" 
                    id="inputTwitter" placeholder="@lang('dash.twitter')"
                    value="{{ isset($row) ? $row->twitter : old('twitter') }}" 
                    name="twitter"
            >
            @error('twitter')
                <small class=" text text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </small>
            @enderror
        </div>
    </div>{{-- twitter --}}
    <div class="form-group">
        <label for="inputSnapchat" class="col-sm-6 col-form-label">@lang('dash.snapchat')</label>
        <div class="col-sm-10 col-lg-10 col-md-2">
            <input  type="text" class="form-control @error('snapchat') is-invalid @enderror" 
                    id="inputSnapchat" placeholder="@lang('dash.snapchat')"
                    value="{{ isset($row) ? $row->snapchat : old('snapchat') }}" 
                    name="snapchat"
            >
            @error('snapchat')
                <small class=" text text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </small>
            @enderror
        </div>
    </div>{{-- snapchat --}}
</div>

<div class="row">
    @foreach (config('translatable.locales') as $index => $locale)
        <div class="form-group">
            <label for="inputName" class="col-sm-6 col-lg-12 col-form-label">@lang('dash.' . $locale . '.name')</label>
            <div class="col-sm-10 col-lg-10 col-md-2">
                <input  type="text" class="form-control @error($locale.'.name') is-invalid @enderror" id="inputName" 
                        placeholder="@lang('dash.' . $locale . '.name')"
                        value="{{ isset($row) ? $row->translate($locale)->name : old($locale . '.name') }}"
                        name="{{ $locale }}[name]"
                >
                @error($locale.'.name')
                    <small class=" text text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </small>
                @enderror
            </div>
        </div>
    @endforeach
</div>{{-- name --}}

<div class="row">
    @foreach (config('translatable.locales') as $index => $locale)
        <div class="form-group">
            <div class="row">
                <label for="inputDescription" class="col-sm-6 col-form-label">@lang('dash.' . $locale . '.description')</label>
                <div class="col-sm-12 col-lg-12 col-md-2">
                    <textarea   name="{{ $locale }}[description]" id="inputDescription" cols="30" rows="10"
                                class="form-control ckeditor @error($locale . '.description') is-invalid @enderror">
                        {{ isset($row) ? $row->translate($locale)->description : old($locale . '.description') }} 
                    </textarea>

                    @error($locale.'.description')
                        <small class=" text text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </small>
                    @enderror
                </div>
            </div>
        </div>
    @endforeach
</div>{{-- description --}}

<div class="row">
    @foreach (config('translatable.locales') as $index => $locale)
        <div class="form-group">
            <label for="inputAbout" class="col-sm-6 col-form-label">@lang('dash.' . $locale . '.about')</label>
            <div class="col-sm-12 col-lg-12 col-md-2">
                <textarea   name=" {{ $locale }}[about_us]" id="inputAbout" cols="30" rows="10"
                            class="form-control ckeditor @error($locale . '.about') is-invalid @enderror">
                    {{ isset($row) ? $row->translate($locale)->about_us : old($locale . '.about_us') }}
                </textarea>

                @error($locale.'.about_us')
                    <small class=" text text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </small>
                @enderror
            </div>
        </div>
    @endforeach 
</div>{{-- about --}}

<div class="row">
    @foreach (config('translatable.locales') as $index => $locale)
        <div class="form-group">
            <label for="inputTerms" class="col-sm-6 col-form-label">@lang('dash.' . $locale . '.terms')</label>
            <div class="col-sm-12 col-lg-12 col-md-2">
                <textarea   name=" {{ $locale }}[terms]" id="inputTerms" cols="30" rows="10"
                            class="form-control ckeditor @error($locale . '.terms') is-invalid @enderror">
                    {{ isset($row) ? $row->translate($locale)->terms : old($locale . '.terms') }}
                </textarea>

                @error($locale.'.terms')
                    <small class=" text text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </small>
                @enderror
            </div>
        </div>
    @endforeach
</div>{{-- terms --}}

<div class="row">
    <div class="form-group">
        <label class="col-sm-6 col-form-label" for="inputLogo">@lang('dash.logo')</label>
        <div class="col-sm-10 col-lg-10 col-md-2">
            <input  type="file" name="logo" id="inputLogo" 
                    class="form-control @error($locale . '.logo') is-invalid @enderror" 
            >
        </div>
        @error('logo')
            <small class=" text text-danger" role="alert">
                <strong>{{ $message }}</strong>
            </small>
        @enderror
    </div> {{-- logo --}}

    <div class="form-group">
        <label class="col-sm-6 col-form-label" for="inputImages">@lang('dash.images')</label>
        <div class="col-sm-10 col-lg-10 col-md-2">
            <input  type="file" name="images[]" id="inputImages" 
                    class="form-control @error($locale . '.images') is-invalid @enderror" 
                    multiple>
        </div>
        @error('images')
            <small class=" text text-danger" role="alert">
                <strong>{{ $message }}</strong>
            </small>
        @enderror
    </div> {{-- images --}}
</div>{{-- medias --}}