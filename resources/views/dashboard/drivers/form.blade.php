{{ csrf_field() }}

<div class="form-group col-md-6 mb-2">
    <div class="form-group">
        <label class="bmd-label-floating">@lang('dash.name')</label>
        <input class="form-control is-invalid  " id="inputTitle"
               placeholder="@lang('dash.name')"
               value="{{ isset($row) ? $row->name : old('name') }}"
               name="name"
        >
        @error('name')
        <span class="text-danger">{{$message}}</span>
        @enderror
    </div>
</div>

<div class="form-group col-md-6 mb-2">
    <label for="">{{__('dash.email')}}</label>
    <input name="email" type="email" id="email" class="form-control @error('email') is-invalid  @enderror"
           @if( !isset($row) ) @endif
           value="{{ isset($row) ? $row->email : old('email') }}"
    >
    @error("email")
    <span class="text-danger">{{$message}}</span>
    @enderror
</div>

<div class="form-group col-md-6 mb-2">
    <label for="">@lang('dash.password')</label>
    <input name="password" type="password" id="password" class="form-control @error('password') is-invalid  @enderror"
    >
</div>

<div class="form-group col-md-6 mb-2">
    <label for="inputAddress">@lang('dash.address')</label>
    <input type="text" id="pac-input" class="form-control" name="address"
           value="{{ isset($row) && !is_null($row->location) ? $row->location->address : old('address') }}"
    >
    @error('address')
    <small class=" text text-danger" role="alert">
        <strong>{{ $message }}</strong>
    </small>
    @enderror
    <input type="hidden" name="latitude" id="latitude" value="0"/>
    <input type="hidden" name="longitude" id="longitude" value="0"/>
</div>

<div class="form-group col-md-6 mb-2">
    <div class="form-group">
        <label for="">{{__('dash.phone')}}</label>
        <input name="phone" class="form-control @error('phone') is-invalid  @enderror"
               value="{{ isset($row) ? $row->phone : old('phone') }}">
    </div>
</div>

<div class="form-group col-md-6 mb-2">
    <label for="inputAge">@lang('dash.profile_image')</label>
    <input type="file" name="profile_image" id="inputProfileImage" class="form-control">
    @error('profile_image')
    <small class=" text text-danger" role="alert">
        <strong>{{ $message }}</strong>
    </small>
    @enderror
</div>

<div class="form-group col-md-6 mb-2">
    <div class="offsetfemale-sm-2 col-sm-10">
        <div class="checkbox">
            <label>@lang('dash.male')
                <input type="radio" name="gender" value="male"
                       @if(isset($row) && $row->gender == 'male') checked @endif > @lang('dash.female')
                <input type="radio" name="gender" value="female"
                       @if(isset($row) && $row->gender == 'female') checked @endif >
            </label>
        </div>
    </div>
</div>

<br>
@push('footer-after-bootstrap')
    <!-- autocomplete google map -->
    <script type="text/javascript"
            src="https://maps.google.com/maps/api/js?key=*****&sensor=false&libraries=places&language=en-AU">
    </script>

@endpush
