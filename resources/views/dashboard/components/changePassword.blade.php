<!-- Modal Edit Profile -->
<div class="modal fade" id="changePassword" tabindex="-1" role="modal" aria-labelledby="Change Password" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('dash.change_password')</h4>
                    <a class="close btn btn-sm mb-0" data-dismiss="modal" aria-label="Close">×</a>
                </div>

                <div class="card-content collpase show">
                    <div class="card-body">
                        <div class="card-text">
                            <p class="card-text" style="margin-bottom: 1.25rem !important;">@lang('dash.change_password_hint')</p>
                        </div>
                        
                        <div class="container-fluid row d-flex justify-content-center ">
                            @if( Session::has('changePasswordErrorMessage') )
                                <div class="alert col-lg-12 col-sm-6 text-center alert-{{session('message_type') == 'success' ?  'success'  :  'warning' }}"
                                        role="alert">
                                    {{session('changePasswordErrorMessage')}}
                                </div>
                            @endif
                        </div><!-- end alert -->

                        <form class="form" method="POST" id="changePasswordForm" action="{{ route('dashboard.change.password') }}">
                            @method('POST')
                            {{ csrf_field() }}
                            <div class="form-body">
                                <div class="form-group">
                                    <label for="current_password"> @lang('dash.current_password') </label>
                                    <input  type="password" id="current_password" class="form-control @error('current_password') is-invalid @enderror" 
                                            placeholder="@lang('dash.current_password_hint')" name="current_password">
                                </div>

                                <div class="form-group">
                                    <label for="new_password">@lang('dash.new_password')</label>
                                    <input  type="password" id="new_password" class="form-control @error('new_password') is-invalid @enderror" 
                                            placeholder="@lang('dash.new_password_hint')" name="new_password">
                                </div>

                                <div class="form-group">
                                    <label for="new_password_confirmation">@lang('dash.new_password_confirmation')</label>
                                    <input  type="password" id="new_password_confirmation" class="form-control @error('new_password_confirmation') is-invalid @enderror" 
                                            placeholder="@lang('dash.new_password_confirmation_hint')" name="new_password_confirmation">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div> 

            <div class="modal-footer" style="border-top: none;">
                <div class="form-actions">
                    <button type="button" class="btn btn-outline-warning mr-1" data-dismiss="modal">
                        <i class="ft-x"></i> @lang('dash.cancel')
                    </button>
                    <button form="changePasswordForm" type="submit" class="btn btn-outline-primary">
                        <i class="ft-check"></i> @lang('dash.save')
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./End Modal Edit Profile -->