<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'بيانات الاعتماد هذه لا تتطابق مع سجلاتنا.',
    'password' => 'كلمة المرور المقدمة غير صحيحة.',
    'throttle' => 'محاولات تسجيل دخول كثيرة جدًا. يرجى المحاولة مرة أخرى في: ثواني.',
    'wrong_password'=>'كلمة السر غير صحيحة.',
    'must_confirm_terms'=>'يجب الموافقة على الشروط ولاحكام لتسجيل فى التطبيق.',
    'phone_exists'=>'رقم الهاتف مسجل من قبل.',
    'phone_not_exists'=>'رقم الهاتف غير مسجل من قبل.',
    'jsut_users_are_avalible'=>'فقط طتبيق المستخدميين متاح حاليا.',
    'account_not_verified'=>'يجب تأكيد رقم الهاتف لتفعيل الحساب.',
    'account_not_active'=>'يجب ان يكون الحساب نشط لتسجيل الدخول,من فضلك قم بالتواصل مع خدمة العملاء لمساعدتك.',
    'unauthenticated' => 'غير مصرح له بالدخول, ربماالحساب غير مفعل او لم يتم تاكيد رقم الهاتف.',
    'account_password_required' => 'يجب ان يتم حفظ كلمة المرور فى حالة الانتقال التلقائي بين الحسابات.',
    
];
