<?php

return [

    /*
    |--------------------------------------------------------------------------
    | APIs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during APIs for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'already_exists' => 'تم أضافته من قبل.',
    'data_updated' => 'تم تحديث البيانات بنجاح.',
    'couldnt_add_product' => 'يجب ان تقوم باضافة منتجات من نفس المتجر الى السلة.',
    'favorite_product_removed' => 'تم الحذف.',
    'provider_location_not_found' => 'موقع المتجر غير موجود.',
    'add_productes_to_order_first' => 'قم باضافة منتجات الى السلة اولا.',

];
