<?php

namespace App\DataTables;

use App\Models\Rate;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class RateDataTable extends DataTable
{
    protected $model;

    /**
     * Constructor.
     */
    public function __construct(Rate $model)
    {
        $this->model = $model;
    }

    /**
     * Build DataTable class.
     * user - order id - provider name - provider rate out of 5 - driver name - driver rate out of 5
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn(__('dash.order_id'), function ($query) {
                $query->id;
            })
            ->addColumn(__('dash.client'), function ($query) {
                return $query->client->name;
            })
            ->addColumn(__('dash.provider'), function ($query) {
                return $query->provider->name;
            })
            ->addColumn(__('dash.rate_provider'), function ($query) {
                return $query->rate_provider;
            })
            ->addColumn(__('dash.driver'), function ($query) {
                return $query->driver->name;
            })
            ->addColumn(__('dash.rate_driver'), function ($query) {
                return $query->rate_driver;
            })
            ->editColumn('created_at', function ($query) {
                return $query->created_at->toFormattedDateString();
            })->addColumn('actions', function ($query) {
                $module_name_singular = 'rate';
                $module_name_plural = 'rates';
                $row = $query;
                return view('dashboard.buttons.show', compact('module_name_singular', 'module_name_plural', 'row'));
            })
            ->filter(function ($query) {
                return $query
                    ->where(function ($w) {
                        return $w->where('id', 'like', "%" . request()->search['value'] . "%")
                            ->orWhereHas('client', function (Builder $c) {
                                $c->where('name', 'like', '%' . request()->search['value'] . '%');
                            })
                            ->orWhereHas('provider', function (Builder $p) {
                                $p->where('name', 'like', '%' . request()->search['value'] . '%');
                            })
                            ->orwhere('rate_provider', 'like', "%" . request()->search['value'] . "%")
                            ->orwhere('rate_driver', 'like', "%" . request()->search['value'] . "%")
                            ->orwhere('created_at', 'like', "%" . request()->search['value'] . "%")
                            ->orwhere('updated_at', 'like', "%" . request()->search['value'] . "%");
                    });
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return $this->model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('ratedatatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id'),
            Column::make(__('dash.order_id')),
            Column::make(__('dash.client')),
            Column::make(__('dash.provider')),
            Column::make(__('dash.rate_provider')),
            Column::make(__('dash.driver')),
            Column::make(__('dash.rate_driver')),
            Column::make('created_at'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Rate_' . date('YmdHis');
    }
}
