<?php

namespace App\DataTables;

use App\Models\Order;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Database\Eloquent\Builder;

class OrderDataTable extends DataTable
{
    protected $model;
	/**
     * Constructor.
     */
    public function __construct(Order $model)
    {
        $this->model = $model;
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $query = $this->query();

        return datatables()
            ->eloquent($query)
            ->addColumn(__('dash.order_id'), function ($query) {
                $query->id;
            })
            ->addColumn(__('dash.client'), function ($query) {
                return $query->client->name;
            })
            ->addColumn(__('dash.provider'), function ($query) {
                return $query->provider->name;
            })
            ->addColumn(__('dash.net_price'), function ($query) {
                return $query->netPrice.__('dash.sar');
            })
            ->editColumn('created_at', function ($query) {
                return $query->created_at->toFormattedDateString();
            })
            ->filter(function ($query) {
                return $query
                    ->where(function ($w) {
                        return $w->where('id', 'like', "%" . request()->search['value'] . "%")
                            ->orWhereHas('client', function (Builder $c) {
                                $c->where('name','like', '%'.request()->search['value'].'%');
                            })
                            ->orWhereHas('provider', function (Builder $p) {
                                $p->where('name','like', '%'.request()->search['value'].'%');
                            })
                            ->orwhere('created_at', 'like', "%" . request()->search['value'] . "%")
                            ->orwhere('updated_at', 'like', "%" . request()->search['value'] . "%");
                    });
            });
    }



    /**
     * Get query source of dataTable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $index = $_REQUEST['index'];
        switch ($index) {
            case 'waiting':
                return $this->model->where('status', 'waiting')->newQuery();
                break;
            case 'processing':
                return $this->model->where('status', 'processing')->newQuery();
                break;
            case 'delivering':
                return $this->model->where('status', 'delivering')->newQuery();
                break;
            case 'finished':
                return $this->model->where('status', 'finished')->newQuery();
                break;
            case 'canceled':
                return $this->model->where('status', 'canceled')->newQuery();
                break;
            case 'refused':
                return $this->model->where('status', 'refused')->newQuery();
                break;
            default:
                return $this->model->where('status', 'timeout')->newQuery();
        }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('orderdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make(__('dash.order_id')),
            Column::make(__('dash.client')),
            Column::make(__('dash.provider')),
            Column::make(__('dash.net_price')),
            Column::make('created_at'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Order_' . date('YmdHis');
    }
}
