<?php

namespace App\DataTables;

use App\Models\Admin;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Database\Eloquent\Builder;

class AdminDataTable extends DataTable
{
    protected $model;

    public function __construct(Admin $model)
    {
        $this->model = $model ;
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $query = Admin::where('id', '>=', 1);
        return datatables()
            ->eloquent($query)
            ->editColumn('name', function ($query) {
                return $query->name;
            })->editColumn('email', function ($query) {
                return $query->email;
            })->editColumn('role', function ($query) {
                $role = is_null($query->adminRole)? __('dash.not_set_yet'):$query->adminRole->role->display_name;
                return $role;
            })->editColumn('is_active', function ($query) {
                return ($query->is_active)?'<spane class="badge bg-info text-uppercase">'.__('dash.yes').'</spane>':'<spane class="badge bg-danger text-uppercase">'.__('dash.no').'</spane>';
            })->editColumn('created_at', function ($query) {
                return $query->created_at->toFormattedDateString();
            })->addColumn('actions', function ($query) {
                $module_name_singular = 'admin';
                $module_name_plural = 'admins';
                $row = $query;
                return view('dashboard.buttons.edit', compact('module_name_singular', 'module_name_plural', 'row')) . view('dashboard.buttons.delete', compact('module_name_singular', 'module_name_plural', 'row'));
            })->filter(function ($query) {
                return $query
                    ->where(function ($w) {
                        return $w->where('name', 'like', "%" . request()->search['value'] . "%")
                            ->orwhereHas('adminRole', function (Builder $r) {
                                $r->role->where('display_name','like', '%'.request()->search['value'].'%');
                            })
                            ->orwhere('email', 'like', "%" . request()->search['value'] . "%")
                            ->orwhere('created_at', 'like', "%" . request()->search['value'] . "%");
                    });
            })
            ->rawColumns(['is_active','actions']);
    }

    /**
     * Get query source of dataTable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return $this->model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('admindatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id'),
            Column::make('name'),
            Column::make('email'),
            Column::make('is_active'),
            Column::make('role'),
            Column::make('created_at'),
            Column::computed('actions')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Admin_' . date('YmdHis');
    }
}
