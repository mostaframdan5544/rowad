<?php

namespace App\DataTables;

use App\Models\Provider;
use App\Models\Rate;
use App\Models\User;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ProviderDataTable extends DataTable
{
    protected $model;

    public function __construct(Provider $model)
    {
        $this->model = $model;
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
           ->addColumn(__('dash.name'), function ($query) {
                return $query->name;
            })->editColumn(__('dash.email'), function ($query) {
                return $query->email;
            })->editColumn(__('dash.Total_Rate'), function ($query) {
                $total_rate = Rate::where('providers_id', $query->id)->avg('rate');
                return $query->total_rate;
            })->editColumn(__('dash.phone'), function ($query) {
                return $query->phone;
            })->editColumn(__('dash.created_at'), function ($query) {
                return $query->created_at->toFormattedDateString();
            })->addColumn('actions', function ($query) {
                $module_name_singular = 'user';
                $module_name_plural = 'users';
                $row = $query;
                return view('dashboard.buttons.show', compact('module_name_singular', 'module_name_plural', 'row')).view('dashboard.buttons.delete', compact('module_name_singular', 'module_name_plural', 'row'));
            })->addColumn('activate', function ($query) {
                $id = $query->id;
                $is_active = $query->is_active;
                return view('dashboard.buttons.active_status', compact('id','is_active'));
            })->filter(function ($query) {
                return $query
                    ->where(function ($w) {
                        return $w->where('name', 'like', "%" . request()->search['value'] . "%")
                            ->orwhere('email', 'like', "%" . request()->search['value'] . "%")
                            ->orwhere('phone', 'like', "%" . request()->search['value'] . "%")
                            ->orwhere('created_at', 'like', "%" . request()->search['value'] . "%");
                    });
            })
            ->rawColumns(['actions','image','activate']);
    }

    /**
     * Get query source of dataTable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */

    public function query()
    {
        return $this->model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('userdatatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id'),
//            Column::make(__('web.image')),
            Column::make(__('dash.name')),
            Column::make(__('dash.email')),
            Column::make(__('dash.Total_Rate')),
            Column::make(__('dash.phone')),
            Column::make(__('dash.created_at')),
            Column::computed('actions')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
            Column::computed('activate')
                ->exportable(false)
                ->printable(false)
                ->width(20)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'User_' . date('YmdHis');
    }


}
