<?php

namespace App\Http\Resources\Api\Drivers;

use Illuminate\Http\Resources\Json\JsonResource;

class DriverResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'phone'         => $this->phone,
            'is_verified'   => $this->is_verified,
            'is_active'     => $this->is_active,
            'image'         => new MediaResource($this->image),
            'address'       => new AddressResource($this->address),
            'device_token'  => $this->device_token,
            // rate
        ];
    }
}
