<?php

namespace App\Http\Resources\Api\Favorites;

use Illuminate\Http\Resources\Json\JsonResource;

class FavoriteGroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'user_id'   => $this->user_id,
            'title'     => $this->title
        ];
    }
}
