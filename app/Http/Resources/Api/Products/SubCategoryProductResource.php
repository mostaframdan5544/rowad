<?php

namespace App\Http\Resources\Api\Products;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Api\MediaResource;

class SubCategoryProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $is_fav = !is_null(auth()->user())? auth()->user()->isFavoriteProduct($this->id) : 0;
        return [
            'id'                =>$this->id,
            'sub_category_name' =>$this->subCategory->name,
            'sub_category_id'   =>$this->subCategory->id,
            'service_name'     =>$this->provider->service_name,
            'provider_id'       =>$this->provider->id,
            'name'              =>$this->name,
            'price'             =>$this->price,
            'image'             =>new MediaResource($this->randamImage()),
            'favorite'          =>$is_fav
            // product rate
        ];
    }
}
