<?php

namespace App\Http\Resources\Api\Products;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Api\MediaResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $is_fav = !is_null(auth()->user())? auth()->user()->isFavoriteProduct($this->id) : 0;
        return [
            'id'                =>$this->id,
            'name'              =>$this->name,
            'description'       =>$this->description,
            'price'             =>$this->price,
            'images'            =>MediaResource::collection($this->medias),
            'favorite'          =>$is_fav
            // product rate
        ];
    }
}
