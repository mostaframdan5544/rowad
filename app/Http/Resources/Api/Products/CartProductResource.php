<?php

namespace App\Http\Resources\Api\Products;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Api\MediaResource;

class CartProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'cart_id'       => $this->id,
            'product_id'    => $this->product->id,
            'service_name'  => $this->product->provider->service_name,
            'description'   => $this->product->description,
            'price'         => $this->price,
            'quantity'      => $this->quantity,
            'image'         => new MediaResource($this->product->randamImage()),
        ];
    }
}
