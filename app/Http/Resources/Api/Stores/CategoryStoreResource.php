<?php

namespace App\Http\Resources\Api\Stores;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Api\MediaResource;

class CategoryStoreResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'provider_id'       => $this->id,
            'service_name'      => $this->service_name,
            'provider_image'    => new MediaResource($this->image)
        ];
    }
}
