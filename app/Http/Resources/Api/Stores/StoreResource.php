<?php

namespace App\Http\Resources\Api\Stores;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Api\Categories\SubCategoryResource;

class StoreResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'name'              => $this->name,
            'sub_categories'    => SubCategoryResource::collection($this->category->subCategories->forpage($_REQUEST['page'],10)),
        ];
    }
}
