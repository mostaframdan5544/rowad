<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class AccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // type must be dynimic
        return [
            'id'                => $this->id,
            'name'              => $this->name,
            'phone'             => $this->phone,
            'type'              => $this->getTable(),
            'is_verified'       => $this->is_verified,
            'is_active'         => $this->is_active,
            'image'             => new MediaResource($this->image),
            'address'           => new AddressResource($this->address),
            'provider_account'  => $this->have_account('providers'),
            'deiver_account'    => $this->have_account('drivers'),
            'user_account'      => $this->have_account('users'),
            'points'            => $this-points,
            'balence'           => $this-balance,
            'total_orders'      => count($this->orders),
            'device_token'      => $this->device_token
        ];
    }
}
