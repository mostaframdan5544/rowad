<?php

namespace App\Http\Resources\Api\AppSettings;

use Illuminate\Http\Resources\Json\JsonResource;

class AppSettingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'description' => $this->description,
            'logo' => $this->logo_path,
            'price_per_km' => $this->price_per_km,
            'delivery_range' => $this->delivery_range,
            'facebook' => $this->facebook,
            'twitter' =>$this->twitter,
            'snapchat' => $this->snapchat,
            'emails' => $this->email,
            'phones' => $this->phone,
            'vat' => $this->vat, // not added to the dashboard yet

        ];
    }
}
