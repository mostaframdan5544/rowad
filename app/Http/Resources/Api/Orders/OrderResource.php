<?php

namespace App\Http\Resources\Api\Orders;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Api\MediaResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $serviceName = is_null($this->provider)? null: $this->provider->service_name;
        return [
            'id'            => $this->id,
            'order_price'   => $this->orderPrice(),
            'provider_id'   => $this->provider_id,
            'service_name'  => $serviceName,
            'status'        => $this->status,
            'image'         => new MediaResource($this->image()),
            'ask_for_driver'=> $this->ask_for_driver,
        ];
    }
}
