<?php

namespace App\Http\Resources\Api\Orders;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Api\AddressResource;

class CheckOutOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $formLocation   = is_null($this->from_locations_id)? null : new AddressResource($this->fromLocation());
        $toLocation     = is_null($this->to_locations_id)? null : new AddressResource($this->toLocation());

        return [
            'id'                => $this->id,
            'from_location'     => $formLocation,
            'to_location'       => $toLocation,
            'vat'               => $this->vat(),
            'driver_cost'       => $this->driverCost(),
            'voucher_discount'  => 0,
            'net_price'         => $this->netPrice(),
            'order_price'       => $this->orderPrice(),
            'ask_for_driver'    => $this->ask_for_driver,
        ];
    }
}
