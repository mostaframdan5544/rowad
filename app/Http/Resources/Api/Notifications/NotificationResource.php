<?php

namespace App\Http\Resources\Api\Notifications;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Api\AskForDrivers\AskForDriverResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $askForDriver = is_null($this->askForDriverResponse) ? null : new AskForDriverResource($this->askForDriverResponse);
        return [
            'notify_id'         => $this->id,
            'content'           => $this->notification->content,
            'from_dashboard'    => $this->from_dashboard,
            'is_read'           => $this->is_read,
            'created_at'        => strtotime($this->notification->created_at),
            'ask_for_driver'    => $askForDriver,
        ];
    }
}
