<?php

namespace App\Http\Requests\Dashboard\AppSetting;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAppSetting extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => 'required|string|email|max:191|unique:app_settings,id,'.$this->id,
            'phone' => 'required|regex:/^\+?\d[0-9-]{9,14}$/|unique:app_settings,id,'.$this->id,
            'logo' => 'nullable|image',
            'imaes' => 'nullable|image',
            'facebook'=>'nullable|min:10|max:191',
            'twitter'=>'nullable|min:10|max:191',
            'snapchat'=>'nullable|min:10|max:191',
            'address' => 'nullable|max:500',
            'latitude' => 'required_with:address|numeric',
            'longitude' => 'required_with:address|numeric',
        ];

        foreach (config('translatable.locales') as $locale) {
            $rules += [
                $locale . '.name'        => 'required|string|min:3|max:200',
                $locale . '.description' => 'nullable|string|min:3',
                $locale . '.about_us' => 'nullable|string|min:3',
                $locale . '.terms' => 'nullable|string|min:3',
            ];
        }

        return [$rules];
    }
}
