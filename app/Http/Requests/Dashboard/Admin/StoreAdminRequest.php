<?php

namespace App\Http\Requests\Dashboard\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:200',
            'email' => 'required|email|unique:admins,email|unique:users,email|unique:drivers,email|unique:providers,email',
            'phone' => 'required|regex:/^\+?\d[0-9-]{9,14}$/|unique:admins,phone|unique:users,phone|unique:drivers,phone|unique:providers,phone',
            'profile_image' => 'nullable|image',
            'is_active'=>'sometimes',
            'password' => 'required|min:9|confirmed',
        ];
    }
}
