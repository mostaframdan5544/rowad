<?php

namespace App\Http\Requests\Dashboard\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:200',
            'email' => 'required|email|unique:users,email|unique:drivers,email|unique:providers,email|unique:admins,id,'.$this->id,
            'phone' => 'required|regex:/^\+?\d[0-9-]{9,14}$/|unique:users,phone|unique:drivers,phone|unique:providers,phone|unique:admins,id,'.$this->id,
            'profile_image' => 'nullable|image',
            'is_active' => 'sometimes',
        ];
    }
}
