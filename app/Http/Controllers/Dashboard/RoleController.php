<?php

namespace App\Http\Controllers\Dashboard;

use App\DataTables\RoleDataTable;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Role;
use App\Models\Admin;
use App\Models\RoleUser;

use Validator;

class RoleController extends BackEndDatatableController
{
    public function __construct(Role $model, RoleDataTable $datatable)
    {
        parent::__construct($model, $datatable);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => ['required', 'min:2', 'max:255', Rule::unique('roles', 'name')],
            'description' => 'required|min:2|max:255',
            'permissions' => 'required'
        ];
        $request->validate($rules);

        $newRole = Role::create([
            'name' => $request->name,
            'display_name' => ucfirst($request->name),
            'description' => $request->description,
        ]);
        $newRole->attachPermissions($request->permissions);

        session()->flash('success', __('dash.the_process_completed_successful'));
        return redirect()->route('dashboard.' . $this->getClassNameFromModel() . '.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => ['required', 'min:2', 'max:255', Rule::unique('roles', 'name')->ignore($id, 'id')],
            'description' => 'required|min:2|max:255',
            'permissions' => 'required'
        ];
        $request->validate($rules);

        $updateRole = $this->model->findOrFail($id);

        $updateRole->update([
            'name' => $request->name,
            'display_name' => ucfirst($request->name),
            'description' => $request->description,
        ]);

        $updateRole->syncPermissions($request->permissions);

        session()->flash('success', __('dash.the_process_completed_successful'));
        return redirect()->route('dashboard.' . $this->getClassNameFromModel() . '.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function userRoleIndex()
    {
        $module_name_plural = "admins_roles";
        $module_name_singular = "admin_role";

        $rows = RoleUser::all();

        return view('dashboard.roles.' . $module_name_plural . '.index', compact('rows', 'module_name_singular', 'module_name_plural'));
    }

    /**
     * Store a newly created resource in storage.
     * Update the specified resource in storage.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function userRoleCreateUpdate(Request $request)
    {
        $isExist = RoleUser::where('user_id', $request->user_id)->where('role_id', $request->role_id)->first();

        $rules = [
            'user_id' => 'required|exists:admins,id',
            'role_id' => 'required|exists:roles,id'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
            return is_null($isExist) ? redirect()->back()->with(["createAdminRoleError" => $validator->errors()->first()])
                : redirect()->back()->with(["id" => $request->user_id . $request->role_id, "updateAdminRoleError" => $validator->errors()->first()]);

        if (is_null($isExist)) {
            if (is_null(RoleUser::where('user_id', $request->user_id)->first()))
                RoleUser::create(['user_id' => $request->user_id, 'role_id' => $request->role_id, 'user_type' => 'App\Models\Admin']);
            else
                RoleUser::where('user_id', $request->user_id)->update(['role_id' => $request->role_id]);

            session()->flash('success', __('dash.the_process_completed_successful'));
        } else
            session()->flash('error', __('dash.admin_have_this_role'));

        return redirect()->back();
    }
}
