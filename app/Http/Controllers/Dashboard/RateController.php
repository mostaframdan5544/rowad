<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Rate;
use App\DataTables\RateDataTable;

class RateController extends BackEndDatatableController
{
    public function __construct(Rate $model, RateDataTable $datatable)
    {
        parent::__construct($model, $datatable);
    }

}
