<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\DataTables\AdminDataTable;
use App\Models\Admin;
use App\Http\Requests\Dashboard\Admin\StoreAdminRequest;
use App\Http\Requests\Dashboard\Admin\UpdateAdminRequest;
use Illuminate\Support\Facades\Hash;
use Validator;

class AdminController extends BackEndDatatableController
{
    public function __construct(Admin $model, AdminDataTable $datatable)
    {
        parent::__construct($model, $datatable);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Dashboard\Admin\StoreAdminRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAdminRequest $request)
    {
        $admin_data = $request->except(['profile_image']);
        $admin_data['password'] = Hash::make($request->password);
        $admin_data['is_active'] = isset($request->is_active)?1:0;
        if( $request->hasFile('profile_image') )
            $admin_data['image'] = $this->uploadImage($request->profile_image, 'admins_medias');

        Admin::create($admin_data);
        session()->flash('success', __('dash.the_process_completed_successful'));
        return redirect()->route('dashboard.adminsroles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $module_name_plural = $this->getClassNameFromModel();
        $module_name_singular = $this->getSingularModelName();

        $row = $this->model->findOrFail($id);

        return view('dashboard.' . $module_name_plural . '.show', compact('module_name_singular', 'module_name_plural', 'row'));
    }

    /**
     * Change user password.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request)
    {
        $user = $this->model->findOrFail(auth()->user()->id);
        $rules = [
            'current_password' => 'required|string',
            'new_password' => 'required|string|min:5|max:50|confirmed'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
            return redirect()->back()->with(["changePasswordErrorMessage" => $validator->errors()->first()]);

        if (Hash::check($request->current_password, $user->password)) {
            $user->update(['password' => Hash::make($request->new_password)]);
        } else
            return redirect()->back()->with(["message_type" => "error",
                "changePasswordErrorMessage" => __('dash.current_password_not_correct')]);

        session()->flash('success', __('dash.the_process_completed_successful'));
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Dashboard\Admin\UpdateAdminRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAdminRequest $request, $id)
    {
        $admin = $this->model->findOrFail($id);

        $admin_data = $request->except(['profile_image']);
        $admin_data['is_active'] = isset($request->is_active)?1:0;
        if( $request->hasFile('profile_image') )
            $admin_data['image'] = $this->uploadImage($request->profile_image, 'admins_medias');

        $admin->update($admin_data);

        session()->flash('success', __('dash.the_process_completed_successful'));
        return redirect()->route('dashboard.' . $this->getClassNameFromModel() . '.index');
    }
}
