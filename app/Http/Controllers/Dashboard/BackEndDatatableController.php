<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;
use Mcamara\LaravelLocalization\LaravelLocalization;
use Yajra\DataTables\Services\DataTable;

use Carbon\Carbon;

class BackEndDatatableController extends Controller
{

    protected $model;
    protected $dataTable;

    public function __construct(Model $model, DataTable $datatable)
    {
        $this->model = $model;
        $this->dataTable = $datatable;
        $this->middleware(['permission:'. $this->getClassNameFromModel().'-read'])->only('index');
        $this->middleware(['permission:'. $this->getClassNameFromModel().'-create'])->only('create');
        $this->middleware(['permission:'. $this->getClassNameFromModel().'-update'])->only('update');
        $this->middleware(['permission:'. $this->getClassNameFromModel().'-delete'])->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        $module_name_plural = $this->getClassNameFromModel();
        $module_name_singular = $this->getSingularModelName();
        return $this->dataTable->render('dashboard.' . $module_name_plural . '.index', compact('module_name_singular', 'module_name_plural'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $module_name_plural = $this->getClassNameFromModel();
        $module_name_singular = $this->getSingularModelName();
        $append = $this->append();

        return view('dashboard.' . $this->getClassNameFromModel() . '.create', compact('module_name_singular', 'module_name_plural'))->with($append);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $module_name_plural = $this->getClassNameFromModel();
        $module_name_singular = $this->getSingularModelName();
        $append = $this->append();
        $row = $this->model->findOrFail($id);
        return view('dashboard.' . $this->getClassNameFromModel() . '.edit', compact('row', 'module_name_singular', 'module_name_plural'))->with($append);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $model = $this->model->findOrFail($id);

        if (isset($model->image)) {
            Storage::disk('public_uploads')->delete($this->getClassNameFromModel() . '_images/' . $model->image);
        }

        $model->delete();
        session()->flash('success', __('site.the_process_completed_successful'));
        return redirect()->route('dashboard.'.$this->getClassNameFromModel() . '.index');
    }

   /**
     * Display the filter resource.
     *
     * @param  collection  $rows
     * @return Collection
     */
    protected function filter($rows)
    {
        return $rows;
    }

    /**
     * Add the append data to response.
     *
     * @return Array
     */
    protected function append()
    {
        return [];
    }

    /**
     * Display class name from model.
     *
     * @return String
     */
    public function getClassNameFromModel()
    {
        return Str::plural($this->getSingularModelName());
    }

    /**
     * Display singular model name.
     *
     * @return String
     */
    public function getSingularModelName()
    {
        return strtolower(class_basename($this->model));
    }

    /**
     * Upload images.
     *
     * @return String
     */

    protected function uploadImage($image, $path)
    {
        $imageName = $image->hashName();
        Image::make($image)->resize(null, 400, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('uploads/' . $path . '/' . $imageName));
        return $imageName;
    }
}
