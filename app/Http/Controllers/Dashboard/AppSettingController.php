<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\AppSetting;
use App\Models\Media;
use App\Models\Location;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Dashboard\AppSetting\UpdateAppSetting;
use DataTables;

class AppSettingController extends BackEndController
{
    protected $model;
    public function __construct(AppSetting $model)
    {
        parent::__construct($model);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $row =  $this->model->first();
        $module_name_plural = $this->getClassNameFromModel();
        $module_name_singular = $this->getSingularModelName();
        $images = [];

        if( !is_null($row) && count($row->medias) )
            $images = $row->medias->where('type', 'image')->pluck('id')->toArray();

        $append = ['images' => $images];

        return  view('dashboard.' . $module_name_plural . '.index', compact('row', 'module_name_singular', 'module_name_plural'))
                ->with($append);
    }

    /**
     * App location.
     *
     * @param \Illuminate\Http\Request $request
     * @param Illuminate\Database\Eloquent\Model $app_set
     * @return void
     */
    public static function appLocation($request, AppSetting $app_set)
    {
        if($app_set->location)
            $app_set->location->update([
                'address'=>$request->address, 'latitude'=>$request->latitude, 'longitude'=>$request->longitude ]);
        else
            Location::create([
                'locationable_type'=> 'App\Models\AppSetting', 'locationable_id'=> $app_set->id,
                'address'=> $request->address, 'latitude'=> $request->latitude, 'longitude'=> $request->longitude ]);
    }

    /**
     * Upload app settings media in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return void
     */
    public function uploadMedia($request, $id)
    {
        $media_data['action_by'] = auth()->user()->id;
        $media_data['mediable_type'] = 'App\Models\AppSetting';
        $media_data['mediable_id'] = $id;
        $media_data['type'] = 'image';

        if( isset($request->images) ){
            foreach($request->images as $image)
            {
                $media_data['path'] = $this->uploadImage($image, 'appsettings_medias');
                Media::create($media_data);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAppSetting $request, $id)
    {
        $appSet = $this->model->findOrFail($id);

        $appset_data = $request->except(['logo', 'images', 'address', 'latitude', 'longitude']);
        $appset_data['action_by'] = auth()->user()->id;

        if ($request->logo) {
            if ($appSet->logo != null) {
                Storage::disk('public_uploads')->delete('appsettings_medias/' . $appSet->logo);
            }
            $appset_data['logo'] = $this->uploadImage($request->logo, 'appsettings_medias');
        }
        $appSet->update($appset_data);

        if(isset($request->address))
            $this->appLocation($request, $appSet);
        
        if(isset($request->images))
            $this->uploadMedia($request, $appSet->id);

        session()->flash('success', __('dash.the_process_completed_successful'));
        return redirect()->route('dashboard.' . $this->getClassNameFromModel() . '.index');
    }

    /**
     * Display app settings images.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $module_name_plural = $this->getClassNameFromModel();
        $module_name_singular = $this->getSingularModelName();
        $row = $this->model->findOrFail($id);

        return view('dashboard.'.$module_name_plural.'.show', compact('module_name_singular', 'module_name_plural', 'row'));
    }

    /**
     * Get app settings images.
     *
     * @return json
     */
    public function images()
    {
        $query = Media::where('mediable_id', request()->appsetting)->where('mediable_type', 'App\Models\AppSetting')
                        ->where('type', 'image');

        return Datatables::of($query)
            ->addColumn('image', function ($query) {
                return '<image src="'.$query->file_path.'" width="40" height="40" style="cursor: url(`'.$query->file_path.'`), pointer;" />';
            })->editColumn('created_at', function ($query) {
                return $query->created_at->diffForHumans();
            })

            ->addColumn('actions', function ($query) {
                $module_name_singular = 'media';
                $module_name_plural   = 'medias';
                $row = $query;
                return view('dashboard.buttons.deleteImage', compact('module_name_singular', 'module_name_plural', 'row'));
            })

            ->filter(function ($query) {
                return $query
                    ->where('mediable_id', request()->appsetting)
                    ->where('mediable_type', 'App\Models\AppSetting')
                    ->where('type', 'image')
                    ->where(function ($w) {
                        return $w->where('id', 'like', "%" . request()->search['value'] . "%")
                            ->orwhere('created_at', 'like', "%" . request()->search['value'] . "%")
                            ->orwhere('updated_at', 'like', "%" . request()->search['value'] . "%");
                    });
            })
            ->rawColumns(['actions', 'image'])
            ->make(true);
    }

    /**
     * Remove the specified app image from storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $media = Media::findOrFail($id);

        Storage::disk('public_uploads')->delete($this->getClassNameFromModel() . '_medias/' . $media->file_path);
        $media->delete();

        session()->flash('success', __('dash.the_process_completed_successful'));
        return redirect()->back();
    }
}
