<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $driverCount       = \App\Models\Driver::count();
        $userCount         = \App\Models\User::count();
        $orderCount        = \App\Models\Order::count();
        $providerCount     = \App\Models\Provider::count();
        $productCount      = \App\Models\Product::count();

        for($i=1; $i<13; $i++){
            $user_count  = \App\Models\User::whereMonth('created_at', '=', $i)->count();
            $u_arr[] = $user_count;

            $driver_count  = \App\Models\Driver::whereMonth('created_at', '=', $i)->count();
            $d_arr[] = $driver_count;

            $provider_count  = \App\Models\Provider::whereMonth('created_at', '=', $i)->count();
            $prov_arr[] = $provider_count;

            $order_count  = \App\Models\Order::whereMonth('created_at', '=', $i)->count();
            $o_arr[] = $order_count;

            $product_count  = \App\Models\Product::whereMonth('created_at', '=', $i)->count();
            $prod_arr[] = $product_count;
        }

        return view('dashboard.home' , compact('driverCount','userCount','orderCount','providerCount','productCount',
                    'u_arr','d_arr','prov_arr','o_arr','prod_arr'
                ));
    }
}
