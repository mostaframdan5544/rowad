<?php

namespace App\Http\Controllers\Dashboard;

use App\DataTables\DriverDataTable;
use App\Http\Requests\UpdateDriverRequest;
use App\Models\Driver;
use App\Models\Location;
use App\Models\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

use Validator;

class DriverController extends BackEndDatatableController
{

    public function __construct(Driver $model, DriverDataTable $datatable)
    {
        parent::__construct($model, $datatable);
    }
    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $module_name_plural = $this->getClassNameFromModel();
        $module_name_singular = $this->getSingularModelName();

        $row = $this->model->findOrFail($id);

        return view('dashboard.' . $module_name_plural . '.show', compact('module_name_singular', 'module_name_plural', 'row'));
    }

    /**
     * Delete events media.
     *
     * @param int $mediable_id
     * @param srting $display_at
     * @return void
     */
    public static function deleteImageIfExist($display_at, $mediable_id)
    {
        Media::where('mediable_id', $mediable_id)->where('mediable_type', 'App\Models\Driver')
            ->where('display_at', $display_at)->delete();
    }

    /**
     * Handle events media in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return void
     */
    public function driverMedia($request, $id)
    {
        $media_data['created_by'] = auth()->user()->id;
        $media_data['mediable_type'] = 'App\Models\Driver';
        $media_data['mediable_id'] = $id;

        if($request->hasFile('profile_image')){
            $this->deleteImageIfExist('profile_image', $id);
            $media_data['display_at'] = 'profile_image';
            $media_data['path'] = $this->uploadImage($request->profile_image, 'drivers_medias');
            Media::create($media_data);
        }
    }

    /**
     * Event location.
     *
     * @param \Illuminate\Http\Request $request
     * @param Illuminate\Database\Eloquent\Model $driver
     * @return void
     */
    public static function driverLocation($request, Driver $driver)
    {
        if($driver->location)
            $driver->location->update([
                'address'=>$request->address, 'latitude'=>$request->latitude, 'longitude'=>$request->longitude ]);
        else
            Location::create([
                'locationable_type'=> 'App\Models\Driver', 'locationable_id'=> $driver->id,
                'address'=> $request->address, 'latitude'=> $request->latitude, 'longitude'=> $request->longitude ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDriverRequest $request, $id)
    {
        $driver_data = $this->model->findOrFail($id);
        $driverset_data = $request->except(['profile_image', 'background',
            'address', 'latitude', 'longitude']);
        $driverset_data['created_by'] = auth()->user()->id;

        // Hash Password
        $driverset_data['password'] = Hash::make('password');

        $driver_data->update($driverset_data);

        if (isset($request->address))
            $this->driverLocation($request, $driver_data);

        if ($request->hasFile('profile_image'))
            $this->driverMedia($request, $driver_data->id);

        session()->flash('success', __('site.the_process_completed_successful'));
        return redirect()->route('dashboard.' . $this->getClassNameFromModel() . '.index');
    }

    public function check($type, $id)
    {
        $record = $this->model::find($id);
        if ($record->$type) {
            $action = "true";
            $record->$type = 0;
        } else {
            $action = "false";
            $record->$type = 1;
        }
        $record->save();
        return response()->json(['status', 200, 'action' => $action]);
    }
}
