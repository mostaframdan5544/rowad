<?php

namespace App\Http\Controllers\Dashboard;

use App\DataTables\RoleDataTable;
use App\DataTables\UserDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use App\Models\User;

use Validator;

class UserController extends BackEndDatatableController
{

    public function __construct(User $model, UserDataTable $datatable)
    {
        parent::__construct($model, $datatable);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $module_name_plural = $this->getClassNameFromModel();
        $module_name_singular = $this->getSingularModelName();

        $row = $this->model->findOrFail($id);

        return view('dashboard.' . $module_name_plural . '.show', compact('module_name_singular', 'module_name_plural', 'row'));
    }

    public function check($type,$id)
    {
        $record= $this->model::find($id);
        if($record->$type){
            $action="true";
            $record->$type=0;
        }else{
            $action="false";
            $record->$type=1;
        }
        $record->save();
        return response()->json(['status',200,'action'=>$action]);
    }
}
