<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\DataTables\OrderDataTable;
use App\Models\Order;
use DataTables;

class OrderController extends BackEndDatatableController
{
    public function __construct(Order $model, OrderDataTable $datatable)
    {
        parent::__construct($model, $datatable);
    }

    /**
     * Display the specified orders for statistics resource.
     *
     */
    public function statistics()
    {
        $query = $this->model->where('id', '>=', 1);

        return Datatables::of($query)
            ->addColumn(__('dash.order_id'), function ($query) {
                $query->id;
            })
            ->addColumn(__('dash.client'), function ($query) {
                return $query->client->name;
            })
            ->addColumn(__('dash.provider'), function ($query) {
                return $query->provider->name;
            })
            ->addColumn(__('dash.driver'), function ($query) {
                return $query->driver->name;
            })
            ->addColumn(__('dash.status'), function ($query) {
                return $query->status;
            })
            ->addColumn(__('dash.net_price'), function ($query) {
                return $query->netPrice.__('dash.sar');
            })
            ->editColumn('created_at', function ($query) {
                return $query->created_at->toFormattedDateString();
            })
            ->filter(function ($query) {
                return $query
                    ->where(function ($w) {
                        return $w->where('id', 'like', "%" . request()->search['value'] . "%")
                            ->orWhereHas('client', function (Builder $c) {
                                $c->where('name','like', '%'.request()->search['value'].'%');
                            })
                            ->orWhereHas('provider', function (Builder $p) {
                                $p->where('name','like', '%'.request()->search['value'].'%');
                            })
                            ->orWhereHas('driver', function (Builder $p) {
                                $p->where('name','like', '%'.request()->search['value'].'%');
                            })
                            ->orwhere('status', 'like', "%" . request()->search['value'] . "%")
                            ->orwhere('created_at', 'like', "%" . request()->search['value'] . "%")
                            ->orwhere('updated_at', 'like', "%" . request()->search['value'] . "%");
                    });
            })
            ->make(true);
    }

}
