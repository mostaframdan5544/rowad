<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Provider;
use App\Models\FavoriteGroup;
use App\Http\Resources\Api\Searches\HomeSearchResource;
use App\Http\Resources\Api\Searches\FavoriteSearchResource;

use Validator;
use Carbon\Carbon;

class SearchController extends Controller
{
    /**
     * Return search results from providers services name or products name.
     *
     * @param \Illuminate\Http\Request $request
     * @return json
     */
    public function home(Request $request) 
    {
        $rules = [
            'key_word' => 'required|string',
            'page' => 'required|integer|min:1',
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $providers_ids = Provider::where('service_name', 'like', "%" . $request->key_word . "%")->pluck('id')->toArray();

        $products = Product::whereIn('providers_id', $providers_ids)->orWhereTranslationLike('name', "%" . $request->key_word . "%")->get();

        return count($products->forpage($request->page,10)) == 0 ? response()->json(['status' =>204]) : 
                response()->json(['status'=>200, 'key_word' => $request->key_word, 'products' => HomeSearchResource::collection($products->forpage($request->page,10))]);
    }

    /**
     * Return search results from favorites groups name.
     *
     * @param \Illuminate\Http\Request $request
     * @return json
     */
    public function favorites (Request $request)
    {
        $rules = [
            'key_word' => 'required|string',
            'page' => 'required|integer|min:1',
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $groups = FavoriteGroup::where('title', 'like',"%" . $request->key_word . "%")->get();

        return count($groups->forpage($request->page,10)) == 0 ? response()->json(['status' =>204]) : 
                response()->json(['status'=>200, 'groups' => FavoriteSearchResource::collection($groups->forpage($request->page,10))]);
    }
}
