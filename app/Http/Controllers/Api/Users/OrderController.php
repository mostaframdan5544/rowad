<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use App\Models\Location;
use App\Http\Resources\Api\Products\OrderProductResource;
use App\Http\Resources\Api\Products\CartProductResource;
use App\Http\Resources\Api\Orders\CheckOutOrderResource;
use App\Http\Resources\Api\Orders\OrderTrackResource;
use App\Http\Resources\Api\Orders\OrderResource;
use App\Http\Resources\Api\Products\CheckOutProductResource;

use Validator;
use Carbon\Carbon;

class OrderController extends Controller
{
    /**
     * Add new cart to order or update cart quantity.
     *
     * @param int $order_id
     * @param \App\Models\Product $product
     * @return void
     */
    public function addOrUpdateCart($order_id, $product)
    {
        $result = Cart::where('order_id',$order_id)->where('product_id',$product->id)->first();
        if(is_null($result))
            Cart::create([
                'order_id'      => $order_id,
                'product_id'    => $product->id,
                'price'         => $product->price,
                'quantity'      => 1
            ]);
        else{
            $result->quantity = $result->quantity+1;
            $result->save();
        }
    }

    public function addProductToOrder(Request $request)
    {
        $rules = [
            'product_id'       => 'required|exists:products,id'
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $product = Product::find($request->product_id);
        if(is_null($product->provider->location))
            return response()->json(['status' => 421 , 'message' => __('api.provider_location_not_found') ]);

        $order = Order::where('user_id', auth()->user()->id)->where('status', 'waiting')->first();
        if(is_null($order))
            $order = Order::create(['user_id'   => auth()->user()->id]);

        if(count($order->carts)){
            $provider_id = $order->carts->first()->product->providers_id;
            if($product->providers_id != $provider_id)
                return response()->json(['status' => 420 , 'message' => __('api.couldnt_add_product') ]);
            
            $this->addOrUpdateCart($order->id, $product);   
        }
        else{
            $this->addOrUpdateCart($order->id, $product); 
            $order->update(['provider_id' => $product->providers_id, 'from_locations_id' => $product->provider->location->id]);
        }

        return response()->json( ['status'  => 200, 'order_id' => $order->id]);
    }

    public function deleteCart(Request $request)
    {
        $rules = [
            'cart_id'       => 'required|exists:carts,id'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        Cart::find($request->cart_id)->delete();

        return response()->json( ['status'  => 200]);
    }

    public function ModifyCartProductQuantity(Request $request)
    {
        $rules = [
            'cart_id'       => 'required|exists:carts,id',
            'quantity'      => 'required|integer|min:1'
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $cart = Cart::find($request->cart_id);
        $cart->quantity = $cart->quantity+$request->quantity; $cart->save();

        return response()->json( ['status'  => 200, 'quantity' => $cart->quantity]);
    }

    /**
     * Return waiting order carts products.
     *
     * @param int $order_id
     * @param \App\Models\Product $product
     * @return void
     */
    public function showCartsProducts (Request $request)
    {
        $rules = [
            'page'       => 'required|integer|min:1'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $order = Order::where('user_id', auth()->user()->id)->where('status', 'waiting')->first();
        if(is_null($order))
            $order = Order::create(['user_id'   => auth()->user()->id]);

        return count($order->carts->forpage($request->page,10)) == 0 ? response()->json(['status' =>204]) : 
            response()->json(['status'=>200, 'products'=>CartProductResource::collection($order->carts->forpage($request->page,10))]);
    }

    public function cancelOrderProcess (Request $request)
    {
        $rules = [
            'order_id'       => 'required|exists:orders,id'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        Order::where('id', $request->order_id)->update(['status' => 'canceled']);

        return response()->json(['status'=>200]);
    }

    /**
     * Add location to order or update it.
     *
     * @param \App\Models\Order $order
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function addOrUpdateOrderToLocation($order, $request)
    {
        if(is_null($order->to_locations_id))
        {
            $location = Location::create([
                'locationable_type'=> 'App\Models\Order', 'locationable_id'=> $order->id,
                'address'=> $request->address, 'latitude'=> $request->lat, 'longitude'=> $request->lon ]);
            $order->to_locations_id = $location->id;
            $order->save();
        }
        else
            Location::where('locationable_type', 'App\Models\Order')
                ->where('id', $order->to_locations_id)->update([
                'address'=>$request->address, 'latitude'=>$request->lat, 'longitude'=>$request->lon ]);
    }

    public function startOrderProcess (Request $request)
    {
        $rules = [
            'address'       => 'required|string|max:200',
            'lat'           => 'required|numeric',
            'lon'           => 'required|numeric',
            'note'          => 'nullable|string|max:200'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $order = Order::where('user_id', auth()->user()->id)->where('status', 'waiting')->first();
        if(is_null($order))
            return response()->json(['status' => 422 , 'message' => __('api.add_productes_to_order_first') ]);
        if(is_null($order->provider->location))
            return response()->json(['status' => 421 , 'message' => __('api.provider_location_not_found') ]);
        
        if(isset($request->note))
            $order->update(['note'=>$request->note]);
        
        $this->addOrUpdateOrderToLocation($order, $request);

        return response()->json(['status'=>200, 'order_details' => new CheckOutOrderResource($order)]);
    }

    /**
     * Update order to_locations_id location object.
     *
     * @param \Illuminate\Http\Request $request
     * @return json
     */
    public function updateOrderToLocation (Request $request)
    {
        $rules = [
            'order_id'       => 'required|exists:orders,id',
            'address'       => 'required|string|max:200',
            'lat'           => 'required|numeric',
            'lon'           => 'required|numeric'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $order = Order::find($request->order_id);

        $this->addOrUpdateOrderToLocation($order, $request);

        return response()->json(['status'=>200, 'order_details' => new CheckOutOrderResource($order)]);
    }

    public function showOrderCheckOutProducts (Request $request)
    {
        $rules = [
            'order_id'  => 'required|exists:orders,id',
            'page'       => 'required|integer|min:1'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }
        
        $order = Order::find($request->order_id);
        return response()->json([
            'status'=>200, 'order_products' => CheckOutProductResource::collection($order->carts->forpage($request->page,10)),
            'order_details' => new CheckOutOrderResource($order)
        ]);
    }

    public function orderTracks (Request $request)
    {
        $rules = [
            'order_id'  => 'required|exists:orders,id'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }
        
        $order = Order::find($request->order_id);
        return count($order->tracks) == 0 ? response()->json(['status' =>204]) : 
            response()->json(['status'=>200, 'time_out'=>$order->time_out, 'tracks' => OrderTrackResource::collection($order->tracks)]);
    }

    public function receivedOrder (Request $request)
    {
        $rules = [
            'order_id'  => 'required|exists:orders,id'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }
        
        $order = Order::find($request->order_id)->update(['status' => 'finished']);

        return response()->json(['status'=>200]);
    }

    public function allOrders (Request $request)
    {
        $rules = [
            'page'       => 'required|integer|min:1'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $orders = Order::where('user_id', auth()->user()->id)->get();
        return response()->json([
            'status'=>200, 'orders' => OrderResource::collection($orders->forpage($request->page,10)) ]);
        
        return count($orders->forpage($request->page,10)) == 0 ? response()->json(['status' =>204]) : 
            response()->json(['status'=>200, 'orders' => OrderResource::collection($orders->forpage($request->page,10)) ]);
    }

    public function showOrder (Request $request)
    {
        $rules = [
            'order_id'  => 'required|exists:orders,id',
            'page'       => 'required|integer|min:1'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }
        
        $order = Order::find($request->order_id);
        return response()->json([
            'status'=>200, 'order_products' => OrderProductResource::collection($order->carts->forpage($request->page,10)),
            'order_details' => new CheckOutOrderResource($order), 'order_status' => $order->status
        ]);
    }

    /**
     * Add location to ask for driver order.
     *
     * @param int $order_id
     * @param \Illuminate\Http\Request $request
     * @return array of int
     */
    public function createAskForDriverLocations ($order_id, $request)
    {
        $from = Location::create([
            'locationable_type'=> 'App\Models\Order', 'locationable_id'=> $order_id,
            'address'=> $request->from_address, 'latitude'=> $request->from_lat, 'longitude'=> $request->from_lon ]);

        $to = Location::create([
                'locationable_type'=> 'App\Models\Order', 'locationable_id'=> $order_id,
                'address'=> $request->to_address, 'latitude'=> $request->to_lat, 'longitude'=> $request->to_lon ]);
        
        return ['from_id' => $from->id, 'to_id' => $to->id];
    }
    
    public function askForDriver (Request $request)
    {
        $rules = [
            'to_address'            => 'required|string|max:200',
            'to_lat'                => 'required|numeric',
            'to_lon'                => 'required|numeric',
            'from_address'          => 'required|string|max:200',
            'from_lat'              => 'required|numeric',
            'from_lon'              => 'required|numeric',
            'note'                  => 'nullable|string|max:200'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $order = Order::create(['user_id'=>auth()->user()->id, 'note'=>$request->note, 'ask_for_driver'=> 1]);

        $locations = $this->createAskForDriverLocations($order->id, $request);

        $order->update(['from_locations_id'=>$locations['from_id'], 'to_locations_id'=>$locations['to_id']]);

        return response()->json(['status'=>200, 'order_id'=>$order->id]);
    }
}
