<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Favorite;
use App\Models\FavoriteGroup;
use App\Http\Resources\Api\Favorites\FavoriteGroupResource;
use App\Http\Resources\Api\Searches\FavoriteSearchResource;

use Validator;
use Carbon\Carbon;

class FavoriteController extends Controller
{

    /**
     * Return all favorites groups.
     *
     * @param \Illuminate\Http\Request $request
     * @return json
     */
    public function all (Request $request)
    {
        $rules = [
            'page'       => 'required|integer|min:1',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $groups = FavoriteGroup::where('user_id', auth()->user()->id)->get();

        return count($groups->forpage($request->page,10)) == 0 ? response()->json(['status' =>204]) : 
            response()->json(['status'=>200, 'groups'=>FavoriteGroupResource::collection($groups->forpage($request->page,10))]);
    }

    public function addGroup (Request $request)
    {
        $rules = [
            'title'         => 'required|max:100'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $result = FavoriteGroup::where('user_id', auth()->user()->id)->where('title',$request->title)->first();
        if(is_null($result))
            $fav_group = FavoriteGroup::create([
                'user_id'   => auth()->user()->id,
                'title'     => $request->title
            ]);
        else
            return response()->json(['status' => 201 , 'message' => __('api.already_exists') ]);

        return response()->json(['status'=>200]);
    }

    public function addProductToGroup (Request $request)
    {
        $rules = [
            'product_id'    => 'required|exists:products,id',
            'group_id'      => 'required|exists:favorite_groups,id'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $result = Favorite::where('group_id',$request->group_id)->where('product_id',$request->product_id)->first();
        if(is_null($result))
            Favorite::create([
                'group_id'      => $request->group_id,
                'product_id'    => $request->product_id,
            ]);
        else
            return response()->json(['status' => 201 , 'message' => __('api.already_exists') ]);

        return response()->json(['status'=>200]);
    }

    public function removeProductFromGroup (Request $request)
    {
        $rules = [
            'product_id'    => 'required|exists:products,id',
            'group_id'      => 'required|exists:favorite_groups,id'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $result = Favorite::where('group_id',$request->group_id)->where('product_id',$request->product_id)->first();
        if(is_null($result))
            return response()->json(['status' => 404 , 'message' => __('api.favorite_product_removed') ]);
        else
            $result->delete();
            
        return response()->json(['status'=>200]);
    }
}
