<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Driver;
use App\Models\Provider;
use App\Http\Resources\Api\AccountResource;
use Illuminate\Support\Str;

use Validator;
use Hash;
use Auth;
use Carbon\Carbon;

class AuthController extends Controller
{
    /**
     * Log login.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response json
     */
    public function Login(Request $request)
    {
        $rules = [
        	'type'          => 'required|in:users,drivers,providers',
            'password'      => 'required|max:9',
            'phone'         => 'required|regex:/^\+?\d[0-9-]{9,11}$/',
            'remember_me'    => 'required|boolean'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }
    
        $credentials['password'] = $request->password;
        $credentials['phone'] = $request->phone;

        if($request->type == 'users')
            $Account = User::where('phone', $request->phone)->first();
        else
            return response()->json(['status' => 400 , 'message' => __('auth.jsut_users_are_avalible') ]);

        if(is_null($Account)) {return response()->json(['status' => 417, 'message' => __('auth.failed') ]);}

        if(Hash::check($request->password, $Account->password) == false) {
            return response()->json(['status' => 418 , 'message' => __('auth.wrong_password') ]);
        }

        if ($token = $this->guard()->attempt($credentials))
        {
            if($request->remember_me)
            {
                $Account->remember_token = Str::random(30);
                $Account->save();
            }

            if(!$Account->is_verified)
                return response()->json(['status' => 401 , 'message' => __('auth.account_not_verified') ]);

            if(!$Account->is_active)
                return response()->json(['status' => 401 , 'message' => __('auth.account_not_active') ]);

            auth()->login($Account);
            auth()->user();

            return $this->respondWithToken($token);
        }
        else    
            return response()->json(['status' => 401 , 'message' => __('auth.failed') ]);
    }

    public function register(Request $request)
    {
        /**
        * @var array
        */
        $rules = [
            'name'          => 'required|min:3|max:50',
            'password'      => 'required|max:20',
            'type'          => 'required|in:users,drivers,providers',
            'phone'         => 'required|regex:/^\+?\d[0-9-]{9,11}$/',
            'accept_terms' => 'required|boolean',
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        if(!$request->accept_terms)
            return response()->json(["status"=> 400, "message" =>  __('auth.must_confirm_terms')]);

        if($request->type == 'users'){
        
            $user_phone = User::where('phone', $request->phone)->first();
            if(!is_null($user_phone))
                return response()->json(["status"=> 410, "message" =>  __('auth.phone_exists')]);

            $user = User::create([
                    'name'      =>$request->name,
                    'password'  =>Hash::make($request->password),
                    'phone'     =>$request->phone,
                    'is_active' =>1,
                    'is_verified'=>0,
                ]);
        }

        //send verification code

        return response()->json( [
            'status'  => 200,
            'account' => new AccountResource($user)
        ]);

    }

    public function verification(Request $request)
    {
        $rules = [
            'code'      => 'required|numeric',
            'phone'     => 'required|regex:/^\+?\d[0-9-]{9,11}$/',
            'type'      => 'required|in:users,drivers,providers',
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        if($request->type == "users"){
            $user = User::where('phone', $request->phone)->first();
            if(is_null($user))
                return response()->json(["status"=> 410, "message" =>  __('auth.phone_not_exists')]);

            // check code validation

            $user->update(['is_verified'=>1]);
        }else
            return response()->json(['status' => 400 , 'message' => __('auth.jsut_users_are_avalible') ]);

        return  response()->json(["status" =>  200, 'account' => new AccountResource($user)]);
    }

    public function resendCode(Request $request)
    {
        $rules = [
            'phone'=> 'required|regex:/^\+?\d[0-9-]{9,11}$/'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        // send another code to the phone

        return  response()->json(["status" =>  200]);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'status'      => 200,
            'user'        => new AccountResource(auth()->user()),
            'access_token'=> $token,
            'token_type'  => 'bearer',
            'expires_in'  => $this->guard()->factory()->getTTL() * 60
        ]);
    }

    public function guard()
    {
        return Auth::guard('users_api');
    }

    public function logout()
    {
        $this->guard()->logout();
        return response()->json(['status' => 200]);
    }

}
