<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AskForDriverResponse;
use App\Http\Resources\Api\AskForDrivers\AskForDriverResource;

use Validator;
use Carbon\Carbon;

class AskForDriverResponseController extends Controller
{
    public function all(Request $request) 
    {
        $rules = [
            'order_id'  => 'required|exists:orders,id',
            'page'      => 'required|integer|min:1',
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $AFDResponses = AskForDriverResponse::where('order_id', $request->order_id)->where('status','waiting')->get();

        return count($AFDResponses->forpage($request->page,10)) == 0 ? response()->json(['status' =>204]) : 
            response()->json(['status'=>200, 'AFDResponses'=>CategoryResource::collection($AFDResponses->forpage($request->page,10))]);
    }

    public function userDecision(Request $request)
    {
        $rules = [
            'ask_for_driver_response_id'    => 'required|exists:ask_for_driver_responses,id',
            'decision'                      => 'required|in:accepted,refused'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $AFDR = AskForDriverResponse::find($request->ask_for_driver_response_id);

        if($request->decision == 'accepted'){
            $AFDR->update(['status' => 'accepted']);

            AskForDriverResponse::where('status', 'waiting')->where('order_id', $AFDR->order_id)->update(['status' => 'refused']);
        }
        else
            $AFDR->update(['status' => 'refused']);

        return response()->json(['status'=>200]);
    }

}
