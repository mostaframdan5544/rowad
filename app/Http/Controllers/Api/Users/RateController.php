<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Rate;

use Validator;
use Carbon\Carbon;

class RateController extends Controller
{
    /**
     * Update rate.
     *
     * @param \App\Models\Rate $rate
     * @param array $request
     * @return void
     */
    public function updateRate($rate, $request)
    {
        $rate->update(['rate'       => $request['rate'], 'comment'    => $request['comment']]);
    }

    public function rateOrder(Request $request)
    {
        $rules = [
            'rate_provider'         => 'required|integer|between:1,5',
            'rate_driver'           => 'required|integer|between:1,5',
            'provider_id'           => 'required|exists:providers,id',
            'driver_id'             => 'required|exists:drivers,id',
            'rate_provider_comment' => 'nullable|string|max:200',
            'rate_driver_comment'   => 'nullable|string|max:200',
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $result = Rate::where('user_id', auth()->user()->id)->where('provider_id', $request->provider_id)->first();
        if(is_null($result))
            Rate::create([
                'user_id'       => auth()->user()->id,
                'provider_id'   => $request->provider_id,
                'rate'          => $request->rate_provider,
                'comment'       => $request->rate_provider_comment,
            ]);
        else
            $this->updateRate($result, ['rate'=>$request->rate_provider, 'comment'=>$request->rate_provider_comment]);

        $result = Rate::where('user_id', auth()->user()->id)->where('driver_id', $request->driver_id)->first();
        if(is_null($result))
            Rate::create([
                'user_id'       => auth()->user()->id,
                'driver_id'     => $request->driver_id,
                'rate'          => $request->rate_driver,
                'comment'       => $request->rate_driver_comment,
            ]);
        else
            $this->updateRate($result, ['rate'=>$request->rate_driver, 'comment'=>$request->rate_driver_comment]);

        return response()->json(['status'=>200]);
    }

    public function rateProduct(Request $request)
    {
        $rules = [
            'rate'          => 'required|integer|between:1,5',
            'product_id'    => 'required|exists:products,id',
            'comment'       => 'nullable|string|max:200',
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $result = Rate::where('user_id', auth()->user()->id)->where('product_id', $request->product_id)->first();
        if(is_null($result))
            Rate::create([
                'user_id'       => auth()->user()->id,
                'product_id'    => $request->product_id,
                'rate'          => $request->rate,
                'comment'       => $request->comment,
            ]);
        else
            $this->updateRate($result, ['rate'=>$request->rate, 'comment'=>$request->comment]);

        return response()->json(['status'=>200]);
    }
}
