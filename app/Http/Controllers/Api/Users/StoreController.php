<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Provider;
use App\Http\Resources\Api\Stores\StoreResource;
use App\Http\Resources\Api\Stores\CategoryStoreResource;

use Validator;
use Carbon\Carbon;

class StoreController extends Controller
{
    public function all(Request $request) 
    {
        $rules = [
            'page' => 'required|integer|min:1',
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $providers = Provider::where('is_active', 1)->get();

        return count($providers->forpage($request->page,10)) == 0 ? response()->json(['status' =>204]) : 
            response()->json(['status'=>200, 'stores' => CategoryStoreResource::collection($providers->forpage($request->page,10))]);
    }

    public function categoryStores(Request $request) 
    {

        $rules = [
            'category_id' => 'required|exists:categories,id',
            'page' => 'required|integer|min:1',
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $providers = Provider::where('category_id', $request->category_id)->where('is_active', 1)->get();

        return count($providers->forpage($request->page,10)) == 0 ? response()->json(['status' =>204]) : 
            response()->json(['status'=>200, 'stores' => CategoryStoreResource::collection($providers->forpage($request->page,10))]);
    }

    /**
     * Return provider resource data.
     *
     * @param \Illuminate\Http\Request $request
     * @return json
     */
    public function show(Request $request) 
    {
        $rules = [
            'provider_id' => 'required|exists:providers,id',
            'page' => 'required|integer|min:1',
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $provider = Provider::find($request->provider_id);

        return response()->json(['status'=>200, 'store'=> new StoreResource($provider)]);
    }

}
