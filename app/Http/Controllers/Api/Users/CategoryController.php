<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\SubCategory;
use App\Http\Resources\Api\Categories\CategoryResource;
use App\Http\Resources\Api\Categories\SubCategoryResource;

use Validator;
use Carbon\Carbon;

class CategoryController extends Controller
{
   
    public function all(Request $request) 
    {

        $rules = [
            'page' => 'required|integer|min:1',
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $categories = Category::where('is_active', 1)->get();

        return count($categories->forpage($request->page,10)) == 0 ? response()->json(['status' =>204]) : 
            response()->json(['status'=>200, 'categories'=>CategoryResource::collection($categories->forpage($request->page,10))]);
    }

    public function subCategories(Request $request) 
    {

        $rules = [
            'category_id' => 'required|exists:categories,id',
            'page' => 'required|integer|min:1',
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }
        
        $category = Category::find($request->category_id);

        return count($category->subCategories->forpage($request->page,10)) == 0 ? response()->json(['status' =>204]) : 
            response()->json(['status'=>200, 'sub_categories'=>SubCategoryResource::collection($category->subCategories->forpage($request->page,10))]);
    }

}
