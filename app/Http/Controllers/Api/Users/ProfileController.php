<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Provider;
use App\Models\Driver;
use App\Models\Location;
use App\Models\Media;
use App\Http\Resources\Api\AccountResource;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use Validator;
use Hash;
use Auth;
use Carbon\Carbon;

class ProfileController extends Controller
{
    public function show (Request $request)
    {
        return response()->json( [
            'status'  => 200,
            'account' => new AccountResource(auth()->user())
        ]);
    }

    public function updateAddress (Request $request)
    {
        $rules = [
            'address'           => 'required|string',
            'lat'               => 'required|numeric',
            'lon'               => 'required|numeric'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        if(is_null(auth()->user()->address))
            Location::create([
                'locationable_type'=> 'App\Models\User', 'locationable_id'=> auth()->user()->id,
                'address'=> $request->address, 'latitude'=> $request->lat, 'longitude'=> $request->lon ]);
        else
            auth()->user()->address->update([
                'address'=>$request->address, 'latitude'=>$request->lat, 'longitude'=>$request->lon ]);

        return response()->json(['status'=>200, 'message' => __('api.data_updated')]);
    }

    /**
     * Upload images.
     *
     * @return String
     */
    protected function uploadImage($image, $path)
    {
        $imageName = $image->hashName();
        Image::make($image)->resize(null, 400, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('uploads/' . $path . '/' . $imageName));
        return $imageName;
    }

    /**
     * Upload app settings media in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function uploadMedia($request)
    {
        $media_data['action_by'] = auth()->user()->id;
        $media_data['mediable_type'] = 'App\Models\User';
        $media_data['mediable_id'] = auth()->user()->id;

        $media_data['path'] = $this->uploadImage($request->image, 'users_medias');
        Media::create($media_data);
    }

    public function updateProfile (Request $request)
    {
        $rules = [
            'name'  => 'required|string',
            'phone' => 'required|numeric',
            'image' => 'nullable|image'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $user = auth()->user();
        $user->update(['name' => $request->name, 'phone' => $request->phone]);

        if ($request->image) {
            if ($user->image != null) {
                Storage::disk('public_uploads')->delete('users_medias/' . $user->image->path);
            }
                $this->uploadMedia($request);
        }

        return response()->json( ['status'  => 200, 'message' => __('api.data_updated'), 'account' => new AccountResource(auth()->user())] );
    }

    protected function respondWithToken($token, $account_type)
    {
        return response()->json([
            'status'      => 200,
            'user'        => new AccountResource(auth()->user()),
            'access_token'=> $token,
            'token_type'  => 'bearer',
            'expires_in'  => $this->guard($account_type)->factory()->getTTL() * 60
        ]);
    }

    public function guard($account_type)
    {
        if($account_type == "providers")
            return Auth::guard('providers_api');
        elseif($account_type == "drivers")
            return Auth::guard('drivers_api');
        else
            return Auth::guard('users_api');

    }

    public function changeAccount (Request $request)
    {
        $rules = [
            'account_type'      => 'required|in:providers,drivers,users',
            'account_password'  => 'nullable|string'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $user = auth()->user();
        if($request->account_type == 'providers')
            $Account = Provider::where('phone', $user->phone)->whereNotNull('remember_token')->first();
        elseif($request->account_type == 'drivers')
            $Account = Driver::where('phone', $user->phone)->whereNotNull('remember_token')->first();
        else
            $Account = User::where('phone', $user->phone)->whereNotNull('remember_token')->first();

        if(is_null($Account))
            return response()->json(['status' => 401 , 'message' => __('auth.unauthenticated') ]);
        else
        {
            if(!isset($request->account_password))
                return response()->json(['status' => 401 , 'message' => __('auth.account_password_required') ]);

            $credentials['password'] = $request->account_password;
            $credentials['phone'] = $user->phone;
            
            if ($token = $this->guard($request->account_type)->attempt($credentials))
            {
                auth()->login($Account);
                auth()->user();

                return $this->respondWithToken($token, $request->account_type);
            }
            else
                return response()->json(['status' => 401 , 'message' => __('auth.failed') ]);
        }
    }
    
}
