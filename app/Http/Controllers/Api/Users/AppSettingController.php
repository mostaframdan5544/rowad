<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Api\AppSettings\AppSettingResource;
use App\Http\Resources\Api\AppSettings\AboutUsResource;
use App\Http\Resources\Api\AppSettings\TermsResource;
use App\Models\AppSetting;

class AppSettingController extends Controller
{
    public function appSettings(Request $request) {

        $appSetting = AppSetting::first();

        return response()->json(['status'=>200, 'app_settings'=> new AppSettingResource($appSetting) ]);

    }

    public function aboutUs(Request $request) {

        $appSetting = AppSetting::first();

        return response()->json(['status'=>200, 'app_settings'=> new AboutUsResource($appSetting) ]);

    }

    public function terms(Request $request) {

        $appSetting = AppSetting::first();

        return response()->json(['status'=>200, 'app_settings'=> new TermsResource($appSetting) ]);

    }
}
