<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Notfication;
use App\Http\Resources\Api\Notifications\NotificationResource;

use Validator;
use Carbon\Carbon;

class NotificationController extends Controller
{
    public function all(Request $request)
    {
        $rules = [
            'page'       => 'required|integer|min:1'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $notifications = auth()->user()->notifies->forpage($request->page,10);
        foreach($notifications as $notify)
            $notify->update(['is_read']);

        return count($notifications) == 0 ? response()->json(['status' =>204]) : 
            response()->json(['status'=>200, 'notifications'=>NotificationResource::collection($notifications)]);
    }

}
