<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ad;
use App\Http\Resources\Api\AdvertisementResource;

use Validator;
use Carbon\Carbon;

class AdvertisementController extends Controller
{
    public function all(Request $request) 
    {
        $rules = [
            'page' => 'required|integer|min:1',
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $ads = Ad::where('is_active', 1)->get();

        return count($ads->forpage($request->page,10)) == 0 ? response()->json(['status' =>204]) : 
            response()->json(['status'=>200, 'ads'=>AdvertisementResource::collection($ads->forpage($request->page,10))]);
    }
}
