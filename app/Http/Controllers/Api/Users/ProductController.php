<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SubCategory;
use App\Models\Provider;
use App\Models\Product;
use App\Models\Carts;
use App\Models\Order;
use App\Models\Location;
use App\Models\FavoriteGroup;
use App\Http\Resources\Api\Products\SubCategoryProductResource;
use App\Http\Resources\Api\Products\ProductResource;
use App\Http\Resources\Api\Products\GroupProductResource;

use Validator;
use Carbon\Carbon;

class ProductController extends Controller
{
    /**
     * Return all products from sub categories.
     *
     * @param \Illuminate\Http\Request $request
     * @return json
     */
    public function subCategoryProducts(Request $request) 
    {
        $rules = [
            'sub_category_id' => 'required|exists:sub_categories,id',
            'page'       => 'required|integer|min:1',
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $sub_category = SubCategory::find($request->sub_category_id);

        return count($sub_category->activeProducts->forpage($request->page,10)) == 0 ? response()->json(['status' =>204]) : 
                response()->json(['status'=>200, 'sub_category_name'=>$sub_category->name, 'subCategoryProducts'=>SubCategoryProductResource::collection($sub_category->activeProducts->forpage($request->page,10))]);
    }

    /**
     * Return top sales products ids for a sub_category.
     * @param int $page
     * @param \App\Models\SubCategory $sub_category
     * @return array
     */
    public function topSalesProducts($sub_category, $page)
    {
        $cart_products_ids = [];
        $sub_category_product_ids = $sub_category->activeProducts->pluck('id')->toArray();

        $order_salied_status = ['processing','delivering','finished', 'canceled'];
        $salid_orders = Order::whereIn('status', $order_salied_status)->get();

        foreach($salid_orders as $salid_order){
            $cart_products_ids = 
            array_push(
                $cart_products_ids, $salid_order->carts->whereIn('product_id', $sub_category_product_ids)->pluck('product_id')->toArray()
            );
        }

        $products = Product::whereIn('id', $cart_products_ids)->get();

        return $products->forpage($page,10);
    }

    /**
     * Return sorted products from sub categories.
     *
     * @param \Illuminate\Http\Request $request
     * @return json
     */
    public function sortSubCategoryProducts(Request $request) 
    {
        $rules = [
            'sub_category_id'   => 'required|exists:sub_categories,id',
            'sort_by'           => 'required|in:top_sales,top_rated,less_price,higher_price,latest,oldest',
            'page'              => 'required|integer|min:1',
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $sub_category = SubCategory::find($request->sub_category_id);

        switch ($request->sort_by) {
            case 'top_sales':
                $products =  ProductResource::collection($this->topSalesProducts($sub_category, $request->page));
                break;
            case 'less_price':
                $products =  ProductResource::collection($sub_category->lessPriceProducts->forpage($request->page,10));
                break;
            case 'higher_price':
                $products =  ProductResource::collection($sub_category->higherPriceProducts->forpage($request->page,10));
                break;
            case 'latest':
                $products =  ProductResource::collection($sub_category->latestProducts->forpage($request->page,10));
                break;
            case 'oldest':
                $products =  ProductResource::collection($sub_category->oldestProducts->forpage($request->page,10));
                break;
            
            // top_rated case not ready yet

        }

        return count($products) == 0 ? response()->json(['status' =>204]) : 
                response()->json(['status'=>200, 'products'=>$products]);
    }

    /**
     * Return product resource data.
     *
     * @param \Illuminate\Http\Request $request
     * @return json
     */
    public function show(Request $request) 
    {
        $rules = [
            'product_id' => 'required|exists:products,id'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $product = Product::find($request->product_id);

        return response()->json(['status'=>200, 'product'=> new ProductResource($product) ]);

    }

    /**
     * Calculates the distance between two points
     * 
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param char $unit
     * @return float Distance between points in $unit
     */
    function distance($lat1, $lon1, $lat2, $lon2, $unit) 
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
      
        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    /**
     * Return nearest providers products ids.
     *
     * @return array
     */
    public function nearestProvidersProductsIds()
    {
        $locations = Location::where('locationable_type', 'App\Models\Provider')->get();
        $providersDistancesFromUser = [];
        $user_lat = auth()->user()->address->latitude;
        $user_lon = auth()->user()->address->longitude;

        foreach($locations as $location)
        {
            $distance = $this->distance($user_lat,$user_lon,$location->latitude,$location->longitude,'k');
            //....
        }
    }

    /**
     * Return filtered products from sub categories.
     *
     * @param \Illuminate\Http\Request $request
     * @return json
     */
    public function filterSubCategoryProducts(Request $request) 
    {
        $rules = [
            'sub_category_id'   => 'required|exists:sub_categories,id',
            'filter_by'         => 'required|in:price,offers,nearest_to_home',
            'price_from'        => 'required_if:filter_by,price|numeric',
            'price_to'          => 'required_if:filter_by,price|numeric',
            'address'           => 'required_if:filter_by,nearest_to_home|string',
            'lat'               => 'required_if:filter_by,nearest_to_home|numeric',
            'lon'               => 'required_if:filter_by,nearest_to_home|numeric',
            'page'              => 'required|integer|min:1',
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $sub_category = SubCategory::find($request->sub_category_id);

        if($request->filter_by == 'price'){
            $products =  ProductResource::collection(
                            $sub_category->productsPriceBetween($request->price_from, $request->price_to)->forpage($request->page,10)
                        );
        }
        elseif($request->filter_by == 'nearest_to_home'){
            //get providers locations that neare to loged user
            //then get thier products under this sub category
            // $providers_products_ids = [1,2,3];
            // $sub_category_products = Product::whereIn('id', $providers_products_ids)->where('sub_category_id', $request->sub_category_id)->get();

            // $products =  ProductResource::collection($sub_category_products->forpage($request->page,10));
        }

        return count($products) == 0 ? response()->json(['status' =>204]) : response()->json(['status'=>200, 'products'=>$products]);
    }

    /**
     * Return all products from providers sub categories.
     *
     * @param \Illuminate\Http\Request $request
     * @return json
     */
    public function storeSubCategoryProducts(Request $request) 
    {
        $rules = [
            'provider_id'       => 'required|exists:providers,id',
            'sub_category_id'   => 'required|exists:sub_categories,id',
            'page'              => 'required|integer|min:1',
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $products = Product::where('sub_category_id', $request->sub_category_id)->where('providers_id', $request->provider_id)->get();
        $sub_category = SubCategory::find($request->sub_category_id);

        return count($products->forpage($request->page,10)) == 0 ? response()->json(['status' =>204]) : 
                response()->json(['status'=>200, 'sub_category_name'=>$sub_category->name, 'products'=>SubCategoryProductResource::collection($products->forpage($request->page,10))]);
    }

    /**
     * Return all products from favorites group.
     *
     * @param \Illuminate\Http\Request $request
     * @return json
     */
    public function favoritesGroupProducts(Request $request) 
    {
        $rules = [
            'group_id'  => 'required|exists:favorite_groups,id',
            'page'      => 'required|integer|min:1',
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
			return response()->json(["status"=> 400, "message" => $validator->errors()->first()]);
        }

        $group = FavoriteGroup::find($request->group_id);
        $group_products_ids = $group->favorites->pluck('product_id')->toArray();
        $products = Product::whereIn('id', $group_products_ids)->where('is_active', 1)->get();

        return count($products->forpage($request->page,10)) == 0 ? response()->json(['status' =>204]) : 
                response()->json(['status'=>200, 'group_title'=>$group->title, 'products'=>GroupProductResource::collection($products->forpage($request->page,10))]);
    }

}
