<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AskForDriverResponse extends Model
{
    use HasFactory;

    /**
     * @var string[] mass assignment
     */
    protected $fillable = [
        'order_id', 'driver_id', 'status', 'price'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function driver() : BelongsTo
    {
        return $this->BelongsTo(Driver::class ,'driver_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order() : BelongsTo
    {
        return $this->BelongsTo(Order::class ,'order_id');
    }
}
