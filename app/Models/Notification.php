<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Database\Factories\OfferFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Notification extends Model
{
    use HasFactory, Translatable;

    protected $table = 'notifications';
    protected $with = ['translations'];
    public $translatedAttributes = ['content'];

    protected $guarded = [];

}
