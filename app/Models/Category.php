<?php

namespace App\Models;

use Database\Factories\CategoryFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;

    public $translatedAttributes = ['name'];

    protected $guarded = [];
    protected $append = ['image_path', 'category_image'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function medias() : MorphMany
    {
        return $this->morphMany(Media::class, 'mediable');
    }

    /**
     * @return string
     */
    public function getCategoryImg()
    {
        $category_imgs = $this->medias->where('display_at', 'category_image')->pluck('id')->toArray();
        $category_img = Media::where('id', $category_imgs[array_rand($category_imgs)])->first();

        return $category_img->file_path;
    }

    public function randamImage()
    {
        $category_imgs = $this->medias->pluck('id')->toArray();
        $category_img = Media::where('id', $category_imgs[array_rand($category_imgs)])->first();

        return $category_img;
    }


    protected static function newFactory()
    {
        return CategoryFactory::new();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subCategories() : HasMany
    {
        return $this->hasMany(SubCategory::class, 'category_id');
    }

}
