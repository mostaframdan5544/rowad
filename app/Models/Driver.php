<?php

namespace App\Models;

use Database\Factories\DriverFactory;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Driver extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'is_active',
        'age',
        'gender',
        'images_id',
        'locations_id',
        'phone',
        'total_rate',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

        // jwt

    /**
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }


    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function medias() : MorphMany
    {
        return $this->morphMany(Media::class, 'mediable');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function location() : MorphOne
    {
        return $this->morphOne(Location::class, 'locationable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function notifies() : MorphMany
    {
        return $this->morphMany(Notify::class, 'notifyable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders() : HasMany
    {
        return $this->hasMany(Order::class, 'driver_id');
    }

    public function have_account($type)
    {
        if($type == 'providers')
            $account = Provider::where('is_active', 1)->where('is_verified', 1)->where('phone', $this->phone)->first();
        elseif($type == 'drivers')
            $account = Driver::where('is_active', 1)->where('is_verified', 1)->where('phone', $this->phone)->first();
        else
            $account = User::where('is_active', 1)->where('is_verified', 1)->where('phone', $this->phone)->first();

        return is_null($account)? 0 : 1;
    }
}
