<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class NotificationTranslation extends Model
{
    use HasFactory;
    protected $table  = 'notifications_translation';

    protected $guarded = [];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notification() : BelongsTo
    {
        return $this->BelongsTo(Notification::class ,'notification_id');
    }
}

