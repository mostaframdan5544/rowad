<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Order extends Model
{
    use HasFactory;

    /**
     * @var string[] mass assignment
     */
    protected $fillable = [
        'user_id', 'driver_id', 'provider_id', 'status', 'from_locations_id', 'to_locations_id', 
        'time_out', 'ask_for_driver', 'nots'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client() : BelongsTo
    {
        return $this->BelongsTo(User::class ,'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider() : BelongsTo
    {
        return $this->BelongsTo(Provider::class ,'provider_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function driver() : BelongsTo
    {
        return $this->BelongsTo(Driver::class ,'driver_id');
    }
    
    public function fromLocation()
    {
        return Location::where('locationable_type', 'App\Models\Provider')->where('id', $this->from_locations_id)->first();
    }

    public function toLocation()
    {
        return Location::where('locationable_type', 'App\Models\Order')->where('id', $this->to_locations_id)->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function carts() : HasMany
    {
        return $this->hasMany(Cart::class, 'order_id');
    }

    public function image ()
    {
        return count($this->carts) == 0? null : $this->carts->first()->product->randamImage();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tracks() : HasMany
    {
        return $this->hasMany(OrderTrack::class, 'order_id');
    }

    /**
     * @return float
     */
    public function vat()
    {
        $appsettings = AppSetting::first();

        return is_null($appsettings->vat)? 0 : $appsettings->vat;
    }

    /**
     * @return float
     */
    public function driverCost()
    {
        $location = new Location();
        $appsettings = AppSetting::first();
        $fromLocation = $this->fromLocation();
        $toLocation = $this->toLocation();
        
        if(is_null($fromLocation) || is_null($toLocation))
            $driverCost = 0;
        else{
            $distance = $location->distance($fromLocation->latitude, $fromLocation->longitude, $toLocation->latitude, $toLocation->longitude, 'k');

            $driverCost = $distance * $appsettings->price_per_km;
        }

        return $driverCost;
    }

    /**
     * @return float
     */
    public function netPrice()
    {
        $netPrice = 0;
        foreach($this->carts as $cart)
            $netPrice += $cart->quantity*$cart->price;
        
        return $netPrice;
    }

    /**
     * @return float
     */
    public function orderPrice()
    {
        return ($this->netPrice() + $this->driverCost() + $this->vat()) - 0;
    }
}
