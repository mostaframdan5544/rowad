<?php

namespace App\Models;

use Database\Factories\UserFactory;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Builder;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = ['name','password','is_active','is_verified','age','gender','phone'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function image() : MorphOne
    {
        return $this->morphOne(Media::class, 'mediable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function address() : MorphOne
    {
        return $this->morphOne(Location::class, 'locationable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function notifies() : MorphMany
    {
        return $this->morphMany(Notify::class, 'notifyable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders() : HasMany
    {
        return $this->hasMany(Order::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function favoriteGroups() : HasMany
    {
        return $this->hasMany(FavoriteGroup::class, 'user_id');
    }

    // jwt

    /**
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * @return bollean
     */
    public function have_account($type)
    {
        if($type == 'providers')
            $account = Provider::where('is_active', 1)->where('is_verified', 1)->where('phone', $this->phone)->first();
        elseif($type == 'drivers')
            $account = Driver::where('is_active', 1)->where('is_verified', 1)->where('phone', $this->phone)->first();
        else
            $account = User::where('is_active', 1)->where('is_verified', 1)->where('phone', $this->phone)->first();

        return is_null($account)? 0 : 1;
    }

    /**
     * @return bollean
     */
    public function isFavoriteProduct($product_id)
    {
        $result = false;

        foreach($this->favoriteGroups as $group)
            foreach($group->favorites as $favorite)
                if($favorite->product_id == $product_id)
                    $result = true;

        return $result;
    }

}
