<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Product extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;

    public $translatedAttributes = ['name', 'description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function medias() : MorphMany
    {
        return $this->morphMany(Media::class, 'mediable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider() : BelongsTo
    {
        return $this->BelongsTo(Provider::class ,'providers_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subCategory() : BelongsTo
    {
        return $this->BelongsTo(SubCategory::class ,'sub_category_id');
    }

    /**
     * @return string
     */
    public function randamImage()
    {
        $product_imgs = $this->medias->pluck('id')->toArray();
        $product_img = Media::where('id', $product_imgs[array_rand($product_imgs)])->first();

        return $product_img;
    }
}
