<?php

namespace App\Models;

use Database\Factories\AdFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Ad extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;

    public $translatedAttributes = ['name'];

    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function medias() : MorphMany
    {
        return $this->morphMany(Media::class, 'mediable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function media() : MorphOne
    {
        return $this->morphOne(Media::class, 'mediable');
    }

    /**
     * @return string
     */
    public function getAdImg()
    {
        $ad_imgs = $this->medias->where('display_at', 'ad_image')->pluck('id')->toArray();
        $ad_img = Media::where('id', $ad_imgs[array_rand($ad_imgs)])->first();

        return $ad_img->file_path;
    }

    protected static function newFactory()
    {
        return AdFactory::new();
    }

}
