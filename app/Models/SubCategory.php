<?php

namespace App\Models;

use Database\Factories\SubCategoryFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\HasMany;

class SubCategory extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;

    public $translatedAttributes = ['name'];

    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function medias(): MorphMany
    {
        return $this->morphMany(Media::class, 'mediable');
    }

    /**
     * @return string
     */
    public function getSubCategoryImg()
    {
        $subcategory_imgs = $this->medias->where('display_at', 'subcategory_image')->pluck('id')->toArray();
        $subcategory_img = Media::where('id', $subcategory_imgs[array_rand($subcategory_imgs)])->first();

        return $subcategory_img->file_path;
    }

    public function randamImage()
    {
        $subcategory_imgs = $this->medias->pluck('id')->toArray();
        $subcategory_img = Media::where('id', $subcategory_imgs[array_rand($subcategory_imgs)])->first();

        return $subcategory_img;
    }

    public function categories()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    protected static function newFactory()
    {
        return SubCategoryFactory::new();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products() : HasMany
    {
        return $this->hasMany(Product::class, 'sub_category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activeProducts() : HasMany
    {
        return $this->hasMany(Product::class, 'sub_category_id')->where('is_active', 1);
    }

    public function getLessPriceProductsAttribute()
    {
        return $this->activeProducts()->getQuery()->orderBy('price')->get();
    }

    public function getHigherPriceProductsAttribute()
    {
        return $this->activeProducts()->getQuery()->orderBy('price', 'desc')->get();
    }

    public function getLatestProductsAttribute()
    {
        return $this->activeProducts()->getQuery()->orderBy('id', 'desc')->get();
    }

    public function getOldestProductsAttribute()
    {
        return $this->activeProducts()->getQuery()->orderBy('id')->get();
    }

    public function productsPriceBetween($priceFrom, $priceTo)
    {
        return $this->activeProducts()->getQuery()->whereBetween('price', [$priceFrom, $priceTo])->get();
    }

}
