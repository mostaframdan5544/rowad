<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SubCategoryTranslation extends Model
{
    use HasFactory;

    protected $table  = 'sub_categories_translation';
    protected $guarded = [];
    public $timestamps = false;

}
