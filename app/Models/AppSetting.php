<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class AppSetting extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;

    /**
     * @var string[]  fillable
     */
    public $translatedAttributes = ['name', 'description', 'about_us', 'terms'];

    protected $guarded = [];

    protected $append = ['logo_path'];

    public function getLogoPathAttribute(){
        return $this->logo != null ? asset('uploads/appsettings_medias/'.$this->logo) : asset('dashboard_files/dist/img/AdminLTELogo.png');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function medias() : MorphMany
    {
        return $this->morphMany(Media::class, 'mediable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function location() : MorphOne
    {
        return $this->morphOne(Location::class, 'locationable');
    }
}
