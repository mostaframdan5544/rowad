<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Notify extends Model
{
    use HasFactory;
    protected $table  = 'notify';
    public $timestamps = false;

    //is_seen
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notification() : BelongsTo
    {
        return $this->BelongsTo(Notification::class ,'notification_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function askForDriverResponse() : BelongsTo
    {
        return $this->BelongsTo(AskForDriverResponse::class ,'ask_for_driver_response_id');
    }

}
