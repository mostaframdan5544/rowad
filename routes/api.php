<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'namespace' => 'App\Http\Controllers\Api',
], function () {

    # USERS APIs
    ##### auth
    Route::post('login',  'Users\AuthController@login');
    Route::post('register',  'Users\AuthController@register');
    Route::post('account/verification', 'Users\AuthController@verification'); // use in register case
    Route::post('resend/code', 'Users\AuthController@resendCode');
    ##### categories
    Route::get('categories/all',  'Users\CategoryController@all');
    Route::get('categories/sub_categories',  'Users\CategoryController@subCategories');
    ##### products
    Route::get('sub_category/products',  'Users\ProductController@subCategoryProducts');
    Route::get('product/show',  'Users\ProductController@show');
    Route::get('sub_category/products/sort',  'Users\ProductController@sortSubCategoryProducts'); // top_rated case not ready yet
    Route::get('sub_category/products/filter',  'Users\ProductController@filterSubCategoryProducts'); // just filter by price case
    Route::get('store/sub_category/products',  'Users\ProductController@storeSubCategoryProducts'); // ...
    ##### app settings
    Route::get('app_settings',  'Users\AppSettingController@appSettings');
    Route::get('about_us',  'Users\AppSettingController@aboutUs');
    Route::get('terms',  'Users\AppSettingController@terms');
    ##### advertisements
    Route::get('ads/all',  'Users\AdvertisementController@all');
    ##### searches
    Route::get('search/home',  'Users\SearchController@home');
    ##### store
    Route::get('stores/all',  'Users\StoreController@all');
    Route::get('category/stores',  'Users\StoreController@categoryStores');
    Route::get('store/show',  'Users\StoreController@show');

    Route::group([
        'middleware' => 'auth:users_api',
    ], function () {
        #### favorites
        Route::get('favorites/groups/all',  'Users\FavoriteController@all');
        Route::post('add/favorites/group',  'Users\FavoriteController@addGroup');
        Route::post('add/favorites/group/product',  'Users\FavoriteController@addProductToGroup');
        Route::post('remove/favorites/group/product',  'Users\FavoriteController@removeProductFromGroup');
        ##### products
        Route::get('favorites/group/products',  'Users\ProductController@favoritesGroupProducts');
        ##### searches
        Route::get('search/favorites',  'Users\SearchController@favorites');
        ##### rates
        Route::post('rate/order',  'Users\RateController@rateOrder');
        Route::post('arate/product',  'Users\RateController@rateProduct');
        ##### profile
        Route::post('profile/change_account',  'Users\ProfileController@changeAccount');
        Route::post('profile/update',  'Users\ProfileController@updateProfile');
        Route::post('profile/update/address',  'Users\ProfileController@updateAddress');
        Route::get('profile/show',  'Users\ProfileController@show');
        #### orders
        Route::post('order/add/product',  'Users\OrderController@addProductToOrder');
        Route::post('order/delete/cart',  'Users\OrderController@deleteCart');
        Route::post('order/modify/cart/product/quantity',  'Users\OrderController@ModifyCartProductQuantity');
        Route::get('order/show/carts/products',  'Users\OrderController@showCartsProducts');
        Route::post('cancel/order/process',  'Users\OrderController@cancelOrderProcess');
        Route::post('start/order/process',  'Users\OrderController@startOrderProcess');
        Route::post('update/order/to_location',  'Users\OrderController@updateOrderToLocation');
        Route::get('order/show/check_out/products',  'Users\OrderController@showOrderCheckOutProducts');
        Route::get('order/tracks',  'Users\OrderController@orderTracks');
        Route::post('received/order',  'Users\OrderController@receivedOrder');
        Route::get('all/orders',  'Users\OrderController@allOrders');
        Route::get('show/order',  'Users\OrderController@showOrder');
        ##### auth
        Route::get('logout',  'Users\AuthController@logout');
        ##### notifications
        Route::get('notifications/all',  'Users\NotificationController@all');
        ##### ask for driver
        Route::post('make/ask_for_driver',  'Users\OrderController@askForDriver');
        Route::get('all/drivers/responses',  'Users\AskForDriverResponseController@all');
        Route::post('user/decision',  'Users\AskForDriverResponseController@userDecision');
        
    });
});
