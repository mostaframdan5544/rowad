<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'App\Http\Controllers\Dashboard\HomeController@index')->name('home');
Route::group(
    [
        'middleware' => ['auth'],
        'namespace' => 'App\Http\Controllers\Dashboard'
    ],
    function () {

        Route::prefix('dashboard')->name('dashboard.')->middleware(['auth'])->group(function () {
            Route::get('/home', 'HomeController@index')->name('home');

            foreach (File::allFiles(base_path('routes/dashboard/')) as $file) {
                require($file->getPathname());
            }
        });
    }
);
