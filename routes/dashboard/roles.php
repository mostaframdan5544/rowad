<?php
    
    Route::resource('roles', 'RoleController');
    
    Route::group([
        'prefix' => 'roles'
    ], function () {
        Route::get('users/roles', 'RoleController@userRoleIndex')->name('adminsroles.index');
        Route::post('users/roles', 'RoleController@userRoleCreateUpdate')->name('adminsroles.createUpdate');
    });