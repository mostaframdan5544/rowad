<?php
    Route::resource('appsettings', 'AppSettingController');
    Route::get('images', 'AppSettingController@images')->name('images');
