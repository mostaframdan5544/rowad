<?php

Route::resource('admins', 'AdminController');

Route::group([
    'prefix' => 'admins'
], function () {
    Route::post('change-password', 'AdminController@changePassword')->name('change.password');
});
