<?php
    Route::resource('orders', 'OrderController');
    Route::get('statistics', 'OrderController@statistics')->name('order.statistics');
